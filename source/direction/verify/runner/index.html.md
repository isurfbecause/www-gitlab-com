---
layout: markdown_page
title: "Category Direction - Runner"
description: "GitLab Runner is the multi-platform execution agent that works with GitLab CI to execute the jobs in your pipelines. View more information here!"
canonical_path: "/direction/verify/runner/"
---

- TOC
{:toc}

## GitLab Runner

GitLab Runner is the flexible, multi-platform execution agent that works with [GitLab CI](/direction/verify/continuous_integration) to execute the jobs in your pipelines and powers the CI build platform on GitLab.com.

Our vision is to eliminate CI build fleet maintenance and operational complexity for DevOps teams.

Depending on the organization's size, DevOps teams may be managing hundreds of Runners. In some cases, that may also mean offering different virtual machine configurations and sizes, which adds operational complexity. So at the heart of this vision, the guiding principle is that DevOps teams should not have to spend enormous amounts of time and effort deploying, configuring, and maintaining Runners. As is the case with the GitLab product strategy,  the Runner's product development strategy must consider and balance the needs of both self-managed and GitLab.com product offerings.  Self-managed customers whose security and compliance requirements dictate that the entire DevOps toolchain runs within their private networks can host the Runner themselves. In addition to public clouds, self-managed customers can deploy and host the Runner on several platforms, including Kubernetes, RedHat OpenShift, and Linux on Z for IBM Z mainframes.  The product strategy for self-managed assumes a future, much like today, where there will continue to be a wide variety in the type of infrastructure stacks organizations deploy and manage in their data centers.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ARunner)
- [Overall Vision](/direction/ops#verify)

## Organizational Adoption Journey

```mermaid
graph LR
   classDef blue fill:#3278ca, color:#fff
   classDef darkblue fill:#225798, color:#fff

   subgraph "CUSTOMER JOURNEY"
      A0[User Actions]:::darkblue
   end
   subgraph "POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"
      A1[Install GitLab]:::blue --> A2(Install the 1st Runner):::blue
   end
   subgraph "SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"
      A2 --> A3(Rollout and start managing more than 1 Runner to support the organization):::blue
   end
   A0-.-A1;
```

```mermaid
graph LR
   classDef red fill:#c92d39, color:#fff
   classDef darkred fill:#9a0814, color:#fff
   classDef blue fill:#3278ca, color:#fff
   classDef darkblue fill:#225798, color:#fff

   subgraph "CUSTOMER JOURNEY"
      A0[Runner Pain Points]:::darkred
   end
   subgraph "POC, ONBOARDING, PRODUCTION ENVIRONMENT SETUP"
      A1[Manual steps to install<br /> and configure first Runner]:::red --> A2(Manual steps to configure additional Runners):::red
   end
   subgraph "SCALE USAGE PHASE: USERS, FEATURES, STAGE ADOPTION, n...n+1 phases"
      A2 --> A3[Runner Token registration<br /> process is manual]:::red
      A3 --> A4[High operational overhead<br /> to maintain a large fleet of Runners]:::red
      A4 --> A5[No instrumentation that<br /> indicates why a Runner<br /> has not started]:::red
   end
   A0-.-A1;
```

![Runner Product Development Categories]( /images/direction/cicd/runner_product_dev_categories.png)

The Runner product development strategy comprises three main focus areas to enable the vision.
1. Runner Core: features and capabilities for the open-source Runner CI execution agent.
1. Runner Cloud: the Runner CI build virtual machines available on GitLab SaaS.
1. Runner Enterprise Management: features and capabilities for DevOps teams to simplify the management of the Runner CI/CD build infrastructure.

## Top Vision Item(s)

### Runner Core

- [Autoscaling replacement for Docker Machine:](https://gitlab.com/groups/gitlab-org/-/epics/2502) Autoscaling of GitLab Runner on Virtual Machines hosted on the major cloud platforms is done today with Docker Machine which is in [maintenance mode](https://forum.gitlab.com/t/docker-machine-is-now-in-maintenance-mode/29381). As such, a key strategic initiative is migrating away from Docker Machine for autoscaling.
- Additional Platforms: A common request we get is to add support for various platforms to the Runner. We've chosen to support the plethora of different systems out there by adding support for a [custom executor](https://docs.gitlab.com/runner/executors/custom.html) that can be overridden to support different needs. In this way platforms like [vSphere](https://gitlab.com/gitlab-org/gitlab-runner/issues/2122), and even custom in-house implementations can be implemented. Additional platforms are primarily being implemented through community contributions, so if you're interested in contributing to GitLab these issues are great ones to get involved with.

### Runner Cloud

- [macOS Build Cloud (Runners) on GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1830) Our goal is to provide customers a GitLab CI solution that delivers exceptional build speed and on-demand scale building applications for Apple products, including iPad, iPhone, Apple Watch, Apple TV, and Mac.
- [Windows Runners on GitLab.com - GA Q1FY22](https://gitlab.com/groups/gitlab-org/-/epics/2162) In Q4FY20, we released the preview of Windows Runners on GitLab.com. We have seen a gradual increase in the adoption and use of this service. Additionally, there has been quite a bit of feedback and requests for enhancements. We plan to add a key feature, Windows autoscaler, which will enable the pre-provisioning of Windows VM's to process CI jobs more efficiently.
- [Linux Runners: Offer multiple GCP VM types and sizes](https://gitlab.com/groups/gitlab-org/-/epics/2426) We currently offer a single VM type, (Google Cloud Platform n1-standard-1 ), for Linux Runners on GitLab.com.  However, the execution duration of certain types of CI jobs can be reduced if they are executed on compute or memory optimized VM's, or general-purpose VM's with more vCPUs and RAM. Offering additional options will also help users who are not interested in managing their own Runners.
- [GitLab.com Runners for Self-Managed (Runners-as-a-Service):](https://gitlab.com/groups/gitlab-org/-/epics/3795) We are also exploring offering to enable organizations to connect self-managed GitLab instances to Runners hosted and fully maintained by GitLab. If you are interested in providing feedback on this concept, you can add comments to this issue, [gitlab#226819](https://gitlab.com/gitlab-org/gitlab/-/issues/226819).

###  Runner Enterprise Management

- [Make CI easy for Self-Managed GitLab users:](https://gitlab.com/groups/gitlab-org/-/epics/2778) Our goal is that users self-managed GitLab should be able to get up and running with CI with as little friction as possible.
- [Runner Enterprise Management Dashboard:](https://gitlab.com/groups/gitlab-org/-/epics/4015) In the Runner Enterprise Management product category, we are starting to collect customer feedback on delivering a first-class enterprise management experience in the GitLab UI for organizations that manage hundreds of Runners.

## What's Next & Why

In the section below, we highlight a few key features that we are working to deliver in the in the next few releases.

### Runner Core

- [Enable `after_script` to run even if the job is canceled by the user
  in GitLab.](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4843)
  In
  [gitlab#15603](https://gitlab.com/gitlab-org/gitlab/-/issues/15603),
  the specified `after_script` will still execute when the job is
  canceled by the user. This is an essential feature for CI as, in some
  cases, it is necessary to run clean-up scripts even if the pipeline
  job has failed or canceled by the user. If the user wants to opt-out
  of running `after_script` for `canceled` jobs they will be able to use
  the `CI_JOB_STATUS` [predefined
  variable](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).
- [Use variable inside other variable](https://gitlab.com/gitlab-org/gitlab-runner/issues/1809) The principal goal of this feature is to enable users to use variables declared in the .gitlab.yml file within other variables in that file.
- [Sticky Runners](https://gitlab.com/gitlab-org/gitlab/-/issues/17497) The MVC Sticky Runner feature will enable a user to specify that all jobs in a pipeline execute on a single GitLab Runner configured to use the [shell executor](https://docs.gitlab.com/runner/executors/shell.html). The goal is to implement a MVC solution that simplifies the passing of intermediate build data between stages in a pipeline.

### Runner Cloud

- [macOS Build Cloud Runners on GitLab.com (Closed Beta)](https://gitlab.com/gitlab-org/gitlab/-/issues/224124) We have heard from several users and community members that need the ability to build macOS CI jobs on GitLab.com without having to set up and maintain self-managed macOS Runners. To address this need, we are working towards a closed beta launch of macOS Runners on GitLab.com.

## Maturity Plan

This category is already at the "Lovable" maturity level (see our
[definitions of maturity levels](/direction/maturity/)). However, it's important to us that we defend our lovable status and provide the most value to our user community.
To that end, we continue to collect and analyze customer and community feedback to understand users' pain points with installing, managing, and using GitLab Runners.
If you have any feedback that you would like to share, you can do so in this [epic](https://gitlab.com/gitlab-org/gitlab/issues/39281).

## Competitive Landscape

GitLab Runner is currently evaluated as part of the comprehensive competitive analysis in the [Continuous Integration category](/direction/verify/continuous_integration/#competitive-landscape)

## Top Customer Success/Sales Issue(s)

For the CS team, the issue [gitlab-runner#3121](https://gitlab.com/gitlab-org/gitlab-runner/issues/3121) where orphaned processes can cause issues with the runner in certain cases has been highlighted as generating support issues.

## Top Customer Issue(s)

- [gitlab-org#4976](https://gitlab.com/gitlab-org/gitlab/-/issues/14976): Runner Priority
- [gitlab-runner#2797](https://gitlab.com/gitlab-org/gitlab-runner/issues/2797): Local runner execution MVC
- [gitlab-runner#1057](https://gitlab.com/gitlab-org/gitlab-runner/issues/1057): Specify root folders for artifacts
- [gitlab-runner#6400](https://gitlab.com/gitlab-org/gitlab-runner/issues/6400): Make environment variables set in before_script available for expanding in .gitlab-ci.yml
- [gitlab-runner#1107](https://gitlab.com/gitlab-org/gitlab-runner/issues/1107): Docker Artifact caching
- [gitlab-runner#3392](https://gitlab.com/gitlab-org/gitlab-runner/issues/3392): Multi-line command output can be un-collapsed in Job terminal output view
- [gitlab-runner#3207](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/3207): Configure docker volumes in .gitlab-ci.yml
- [gitlab-runner#1797](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/1797): Caching is very slow for Node.JS projects
