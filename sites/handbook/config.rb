#--------------------------------------
# Monorepo-related configuration
#--------------------------------------

monorepo_root = File.expand_path('../..', __dir__)

# hack around relative requires elsewhere in the shared code by adding the monorepo root to the load path
$LOAD_PATH.unshift(monorepo_root)

set :data_dir, "#{monorepo_root}/data"
set :helpers_dir, "../../helpers" # This has to be relative, because Middleman's ExternalHelpers#after_configuration uses File.join(app.root, ...)

require_relative '../../extensions/monorepo.rb'
activate :monorepo do |monorepo|
  monorepo.site = 'handbook'
end

#--------------------------------------
# End of Monorepo-related configuration
#--------------------------------------

#----------------------------------------------------
# Config which was originally duplicated from top-level
#----------------------------------------------------

require 'extensions/only_debugged_resources'
require "lib/homepage"
require 'lib/mermaid'
require 'lib/plantuml'
require 'lib/code_owners'

# Settings
set :haml, { format: :xhtml }
set :markdown_engine, :kramdown
set :markdown, tables: true, hard_wrap: false, input: 'GFM'

# Disable HAML warnings
# https://github.com/middleman/middleman/issues/2087#issuecomment-307502952
Haml::TempleEngine.disable_option_validator!

# Paths with custom per-page overrides
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# Don't render or include the following into the sitemap
ignore '**/.gitkeep'

# Extensions
activate :syntax, line_numbers: false
if ENV['MIDDLEMAN_DEBUG_RESOURCE_REGEX']
  ::Middleman::Extensions.register(:only_debugged_resources, OnlyDebuggedResources)
  activate :only_debugged_resources
end

# Build-specific configuration
configure :build do
  Kramdown::Converter::PlantUmlHtmlWrapper.plantuml_setup

  # Mermaid diagrams don't render without line breaks
  activate :minify_html, preserve_line_breaks: true
end

#----------------------------------------------------
# End of config which was originally duplicated from top-level
#----------------------------------------------------

#---------------------------------------------
# Common config which is specific to this site
#---------------------------------------------

# Hiring chart pages
Gitlab::Homepage::Team.new.unique_departments.merge!(company: 'Company').each do |slug, name|
  proxy "/handbook/hiring/charts/#{slug}/index.html", "/handbook/hiring/charts/template.html", locals: { department: name }, ignore: true
end

# Compensation Roadmaps
data.compensation_roadmaps.each do |compensation_roadmap|
  proxy "/handbook/engineering/compensation-roadmaps/#{compensation_roadmap.slug}/index.html", "/handbook/engineering/compensation-roadmaps/template.html", locals: {
    compensation_roadmap: compensation_roadmap
  }, ignore: true
end

# GitLab Projects
proxy '/handbook/engineering/projects/index.html',
      '/handbook/engineering/projects/template.html',
      locals: { team: Gitlab::Homepage::Team.new },
      ignore: true

#----------------------------------------------------
# End of common config which is specific to this site
#----------------------------------------------------

#---------------------------------------------------
# Development config which is specific to this site
#---------------------------------------------------

configure :development do
  # There is no root index.html in handbook sub-site, so redirect root to handbook/index.html
  redirect "index.html", to: "/handbook/index.html"

  # Reload the browser automatically whenever files change
  activate :livereload unless ENV['ENABLE_LIVERELOAD'] != '1'

  # External Pipeline
  unless ENV['SKIP_EXTERNAL_PIPELINE']
    # NOTE: This only applies to 'development' mode.  For local builds, use the `rake build:*` tasks
    activate :external_pipeline,
             name: :frontend,
             command: "cd #{monorepo_root} && #{monorepo_root}/source/frontend/pipeline.sh #{build? || environment?(:test) ? '' : ' --watch'}",
             source: "#{monorepo_root}/tmp/frontend",
             latency: 3
  end

  activate :autoprefixer do |config|
    config.browsers = ['last 2 versions', 'Explorer >= 9']
  end

  activate :minify_css
  activate :minify_javascript
end

#----------------------------------------------------------
# End of development config which is specific to this site
#----------------------------------------------------------
