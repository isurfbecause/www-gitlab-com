---
layout: handbook-page-toc
title: "GitLab onboarding and machine management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Onboarding

The GitLab IT team is here to help you through any onboarding struggles you might be dealing with. We have a weekly [IT Onboarding Q&A](https://calendar.google.com/calendar/r/eventedit/M3VpajRiNTdmYXM3ZnViZDI1MGVnaHFvdTRfMjAyMDA1MDVUMTYwMDAwWiBtZGlzYWJhdGlub0BnaXRsYWIuY29t) for GitLab new hires to join and discuss any technical issues or concerns they are having during onboarding. Please also refer to [The guide to remote onboarding](https://about.gitlab.com/company/culture/all-remote/onboarding/) for additional onboarding guidance.

## Laptops

### Laptop Ordering Process

The laptop ordering process starts as soon as an offer is accepted by a candidate and the initial Welcome email is sent by the Candidate Experience Specialist.
This email will include a link to the Notebook Order Form where the new team member will state their intent for obtaining or ordering hardware.

Team members that live in these countries can be serviced via the IT Laptop Ordering Process:

USA, Canada, Japan, Mexico, all of the EU, Russia, Thailand, China, Philippines, Australia, and New Zealand.

Please note that we are adding supported countries to this list as we discover our ability to order in them.
You can test this by going to order a MacBook Pro (or Dell) from the regional Apple store, and seeing if they let you customize a build or alternately refer you to local retailers.
If the latter, see below.

Team members that do not live in these countries will need to procure their own laptop and submit for reimbursement.
If the team member desires financial assistance to purchase the hardware, the Company will advance the funds to help facilitate the purchase (see Exception Processes below).

### Key Performance Indicators

KPI 99% of laptops will arrive prior to start date or 21 days from the date of order.

### Exception Processes

If you are in a region where we are not able to have a laptop delivered, and you need to request funds be advanced in order for a local purchase to take place;
Obtain two quotes from local retailers (online or physical).

Email your manager with those quotes attached, requesting the funds advance and detailing the reason why (geo region, unable to have laptop delivered).
Your manager will then forward their approval to Accounting for final approval and dispensation.

Should a laptop not be available to a new GitLab team-member upon their start date, but is pending, interim options include:

    - Using personal non-windows hardware (mac, linux, mobile)
    - Renting and expensing non-windows hardware
    - Purchasing and expensing (or returning) a Chromebook

If in the rare case that your laptop arrives damaged or unusuable prior to your start date, please reach out to your Candidate Experience Specialist and CC [itops@gitlab.com](mailto:itops@gitlab.com) for next steps on an immediate replacement or repair.

### Laptop Configurations

GitLab approves the use of Apple and Linux operating systems, Windows is prohibited.
The prohibition on Microsoft Windows is for the following reasons:

- While there has been security issues with all operating systems, due to the popularity of the Windows operating system it is the main platform targeted by attackers with spyware, viruses, and ransomware.
- macOS is pre-loaded onto Apple hardware.
    Linux is free.
    To take advantage of the features in Windows as a business, GitLab would have to purchase Windows Pro licensing as these business features are not available on Windows Home Edition.
    As many purchases of laptops have occurred with employees making the purchases and then being reimbursed by GitLab, a remote employee would typically be making a purchase of a laptop pre-loaded with Windows Home Edition.
- Windows Home Edition is notoriously [hard to secure](https://www.markloveless.net/blog/2019/1/15/dealing-with-windows-10).

Further information on GitLab authorized operating systems, versions, and exception process is available on the [Approved OS's](/handbook/security/approved_os.html) page.

The operating system choices have obviously affected the hardware selection process.

Apple hardware is the common choice for most GitLab team-members, but if you are comfortable using and self-supporting yourself with Linux (Ubuntu usually) you may also choose from the Dell builds below.

NOTE: GitLab does have a corporate discount with Apple.
In Apple Retail Stores, you can bring a paystub or invoice along with a photo ID to get
the discount.
You can also call the [Small Business line](https://www.apple.com/retail/business/) to learn more about
ordering online.
Discounts vary depending on the purchase, but it can range anywhere from 2-10%.

##### Apple Hardware

- For Engineers, Support Engineers, Data Analysts, Technical Marketing Managers, Product Designers, UX Managers, Product Managers, Technical Writers, Digital Production (MacBook Pro) - [16”  / 512gig SSD / 32gig RAM / i9 or i7 CPU](https://www.apple.com/shop/buy-mac/macbook-pro/16-inch-space-gray-2.6ghz-6-core-processor-512gb)

- Everyone Else (MacBook Pro) - [13” / 256gig SSD / 16gigs RAM / Quad-Core i5 CPU](https://www.apple.com/shop/buy-mac/macbook-pro/13-inch-space-gray-1.4ghz-quad-core-processor-with-turbo-boost-up-to-3.9ghz-256gb)

##### Linux Hardware

Please only request a Linux laptop if you have experience with this operating system and its requirements, all others, select a MacBook.
{: .alert .alert-info}

We strongly encourage Macs, but we do allow Linux if you are capable of self-support and updates.

-  \*\* For Engineers, Support Engineers, Data Analysts, Technical Marketing Managers, Product Designers, UX Managers, Product Managers, Technical Writers, Digital Production (Dell/Linux) - [16”  / 512gig SSD / 32gig RAM / i9 or i7 CPU ](https://www.dell.com/en-us/work/shop/dell-laptops-and-notebooks/precision-5540-mobile-workstation/spd/precision-15-5540-laptop/xctop554015us4?configurationid=01b3d81d-57ff-4f71-a79f-f6b81ab2a521)

- Everyone Else (Dell/Linux) - [13” / 256gig SSD / 16gigs RAM / Quad-Core i5 CPU](https://www.dell.com/ng/business/p/latitude-13-7300-laptop/pd)

\*\*NOTE: Max price: **the price of the equivalent [16” MacBook pro laptop](#apple-hardware)**. Please make sure you order this model minimum 14 days, based on your locality, prior to your desired date to receive.

\*\* NOTE: [1Password](/handbook/security/#1password-guide) does not yet have a native client for Linux, but [there is a browser extension](https://support.1password.com/getting-started-1password-x/).

For Linux laptops, our **standard laptop provider is Dell** which comes pre-loaded with Ubuntu Linux (to save money by not purchasing a Windows license).
The reasons for using Dell for a Linux laptop are as follows:

- There are several manufacturers of laptop systems that offer Linux, but Dell is the only major manufacturer that has done so for years, and it has already worked out shipping issues for all of the countries where GitLab employees live.

- As we move forward with Zero Trust networking solutions, we need to have a stable and unified platform for deployment of software components in the GitLab environment.
    Standardization on a single platform for Linux simplifies this.

- Ubuntu 18.04 LTS is the preferred platform due to both stability and an extremely fast patch cycle - important for security patches.

- There are opportunities for corporate discounts in the future if we can concentrate purchases from a single vendor.

- Dell is a [certified Ubuntu vendor](https://certification.ubuntu.com/desktop/models?query=&category=Desktop&category=Laptop&level=&release=18.04+LTS&vendors=Dell) with plenty of laptop choices available. They even have their own Ubuntu OEM release of Ubuntu they maintain, and as a result of their effort, the standard Ubuntu 18.04 LTS image natively supports Dell hardware and even firmware updates.

- To date, all of Dell's security issues have involved their use of Windows, not their hardware.

Laptops are purchased by IT Ops when a team-member comes on board; the team-member will be sent a form to fill out for ordering.

### Laptop Vendor Selection Criteria

When recommending or approving end user device vendors for team member access, the Security Team tries to balance privacy, security, and compliance to ensure a solid choice for accessing GitLab data.
Our current recommendations include Apple MacBook Pro running macOS and Dell Precision running Linux.

By its very nature, GitLab has historically been very open as a company, starting as open source and migrating from a group of coders with their own laptops to an organization that needs to protect not just their own corporate data but customer data as well.
Having developed a Data Classification Policy and currently implementing Zero Trust, we've had to make adjustments in laptop recommendations.
Our laptop vendor selection criteria is as follows:

##### Team member need

The main needs center around processing power and the operating system support for required workloads.
Most modern systems meet the processing power needs of our team members.
Apple macOS and Dell Linux distributions meet the operating system needs.

##### Security needs

GitLab needs the ability to ensure a secure and stable platform.
From an operating system perspective, macOS and Linux meet the basic needs.
The Security team has found a slight advantage in Ubuntu as a Linux distribution due to their rapid response time when it comes to patching security flaws, and recommend this distribution.
This is not at the exclusion of all other distributions, but this is the one we recommend.

In the case of Microsoft Windows, as we previously stated there are a number of reasons to restrict access from a security perspective.
Historically, the operating system has had its share of security flaws and is a frequent target of various forms of malware.
In fact Windows is responsible for the major share of all malware, including such items as ransomware.

From a hardware point of view, we have to examine security issues such as supply chain attacks.
Some vendors have had these issues involving the operating system, although in many cases it has been directly related to Microsoft Windows.
However, if a vendor has had potential supply chain issues involving firmware or hardware, we will consider other vendors first.

##### Compliance needs

To meet compliance needs for the various certifications, programs, and industry regulations, we have to meet criteria including the ability to restrict access to sensitive data to company-issued laptops running company-monitored software.
In many cases we need to be able to prove this via auditing, including outside auditors.
Using one vendor for macOS (Apple by default) and one for Linux.
A part of this process will include ensuring systems are patched, and in the Linux case we want to ensure firmware patches are applied.
Very few hardware vendors not only supply Linux as an operating system but also provide a way to apply patches - including security patches - to firmware via the normal Linux patch process.

There is no specific example for using one brand over another from a compliance perspective.
That being said, there are customers we wish to sell GitLab products that have specific requirements internally, and to align ourselves with those requirements can be not only a positive sign we understand the customer space, but give us a competitive advantage.
For example, since there is a [strong push](/solutions/public-sector/) to sell to agencies within the US Government, we will already face restrictions such as support from only US-citizen GitLab team members while on US soil.
As these agencies also have access to classified (non-public) reports on such things as computer vendors, one only has to note which laptops are approved for purchase.
For that reason, we will restrict our vendor list to vendors that are currently approved by the various organizations issuing those certifications and programs we are trying to be compliant with.
This simplifies the ability to support those customers which may impose restrictions on team members working in support roles for that customer solely based upon the hardware they are using.
In other words, we eliminate this possibility of becoming a situation to be managed.

##### Logistics needs

To be able to use a laptop vendor, we have to be able to purchase and ship hardware to our team members regardless of where they live.
Therefore the vendor should be able to handle most if not all shipping requirements to all team members. Our current hardware provider for US and most international locations is CDW. GitLab laptops that are procured from CDW will come with GitLab branded asset labels by default. Please refer to this issue for more information on GitLab asset labels. [GitLab Branded Laptop Labels](https://gitlab.com/gitlab-com/business-ops/team-member-enablement/it-ops-issue-tracker/-/issues/352)

### Laptop Refresh

Team member Laptops can be refreshed after 3 years (of use!) without question. However, if the replacement laptop is outside the standardized specifications listed [here](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#apple-hardware) than manager approval will be required before IT can purchase the replacement laptop.
If you feel you need a new laptop to be effective at your job before then, reach out to IT and your manager.

Replacement laptops for broken GitLab laptops can be purchased as needed by [creating an issue](https://gitlab.com/gitlab-com/business-ops/team-member-enablement/issue-tracker/-/issues/new?issuable_template=Laptop_Replacement) in the Team member Enablement issue tracker project.  Laptops ordered as part of the refresh program use the same template.

This process can also be followed for laptops that are not broken but old enough that you are having trouble completing your work.
Please refer to the [spirit of spending company money](/handbook/spending-company-money/) when deciding whether or not it is appropriate to replace your functioning laptop.
Everyone's needs are different so it is hard to set a clear timeline of when computer upgrades are necessary for all team-members, but team-members become eligible for an updated laptop after 3 years.
If you qualify/complete a laptop refresh, please also refer to our [Laptop Buy back Policy](/handbook/business-ops/team-member-enablement/onboarding-access-requests/#laptop-buy-back-policy) below.

Many team members can use their company issued laptop until it breaks.
If your productivity is suffering, you can request a new laptop.
The typical expected timeframe for this is about three years, but it can depend on your usage and specific laptop. Laptops paid for by the company are property of GitLab and need to be reported with serial numbers, make, model, screen size and processor to IT Ops by adding it to this form: [GitLab laptop information](https://forms.gle/czgiZAT2D1Rxt7Dw6) for proper [asset tracking](/handbook/finance/accounting/#asset-tracking).
Since these items are company property, you do not need to buy insurance for them unless it is company policy to do so (for example, at the moment we do not purchase Apple Care), but you do need to report any loss or damage to IT Ops as soon as it occurs.
Links in the list below are to sample items, other options can be considered.

### Laptop Trade-In

If you have a laptop that is older than 3 years and you intend to
refresh it with a new one, we advise that you trade it in for credit
towards a new laptop.
There are two ways this can be done:

1. [Apple's trade-in program](https://www.apple.com/shop/trade-in).
1. [SellYourMac](https://www.sellyourmac.com/)

Note that Apple's trade-in program requires sending in the laptop within
14 days of requesting a trade-in.
Please also make sure the credit is applied towards a new laptop, as the intent of this policy is to save
the company money and to encourage recycling.

Please check with the IT Ops department if you have any questions.

### Laptop Repair

If your laptop is broken and needs to be repaired you can take it into an Apple repair store.
If the repair is not going to be too expensive (more than $1000 dollars USD), go ahead and repair and expense.
If the repair is going to take longer than a day then you need to make sure you have a back up laptop to work on that is non-Windows.
You can open an issue in the [Team Member Enablement Issue Tracker](https://gitlab.com/gitlab-com/business-ops/team-member-enablement/issue-tracker/-/issues/new?issuable_template=Laptop_Repair) to document the repair and get your managers approval.
If, however, the repair is going to be expensive and take weeks to fix and you have no back up laptop, your best option is to replace the laptop.
In this case please open [an issue to replace](https://gitlab.com/gitlab-com/business-ops/team-member-enablement/issue-tracker/-/issues/new?issuable_template=Laptop_Replacement).
Then please follow the guidelines in the template and once you receive the new laptop we can have the old one sent off to our reseller.

### Configuring New Laptops & Apple IDs

New laptops should be configured with security in mind.

We require the use of an @gitlab.com Apple ID that is separate from any personal Apple ID's you may have.
Some of these reasons include:

- Backups, keychains and documents are all considered sensitive information, and should not be stored in personal services.
- 2FA for remote lock, wipe, or account resets are common methods of account compromises, and ensuring the use of GitLab.com email addresses also ensures we are in control of that aspect of multi-factor authentication.
- Keeping a strong separation between work and personal accounts will help prevent the accidental leak of information from one to the other, in either direction.

Defense in depth, in part, means you make a best effort to be secure at each layer. To read through more instructions, please refer to [security best practices](/handbook/security/#best-practices) when configuring your new laptop.

**All team-members must provide proof of whole-disk encryption within the new laptop order issue.**

Certain circumstances (world region and availability of hardware) might require the self installation of Linux on a Dell that was shipped with OEM Windows.
Please make sure you follow any needed requirements when self installing and open an issue with IT-Ops for verification.

For laptops shipped with OEM Windows you may want to make a full drive backup (e.g. by using open source utility [Clonezilla](https://clonezilla.org/)) to the external drive before installing Linux.
That way you could restore your laptop to the original state at any time.
It will make the RMA process much easier in case you need it.

### Laptop Buy back Policy

Team members can choose to refresh their laptop, no questions asked, after 3 years of use (not necessarily 3 years of employment if a used laptop was issued at the time of onboarding).

Team members have the option to buy back their existing laptops either when it gets refreshed for a new one, or when the team member is offboarding.
If the team member has completed 1 calendar year or more at GitLab at the time of offboarding, they can opt to keep their laptop at no cost.
If the team member hasn't completed 1 calendar year at the time of offboarding or is receiving an early laptop refresh, they have the option to purchase their laptop for current market value from Gitlab.

IT Ops will email the team member asking if they would like to send back or purchase their laptops.
If purchasing, our Manager of IT, or Lead, IT Analyst will approve, and we will send the employee an email with the determined value.
Then, if the employee decides to move forward with purchasing, our accounting department will reach out with payment information.

If a team member decides to retain their laptop, they are required to wipe the machine and re-install the base operating system, and remove any and all software and configurations that were supplied by GitLab.
Evidence or a declaration that the device has been wiped must be supplied to GitLab within 2 weeks of the end of employment.
If GitLab discovers that a device has not been wiped according to policy, GitLab may act to enforce a remote wipe without notice.

If team members opt not to keep or purchase their existing laptops, they can return them to GitLab.
See the [returning old/offboarded laptops](#returning-oldoffboarded-laptops) section below for details.

### Returning Old/OffBoarded Laptops

Part of the IT Ops replacement laptop process is providing each team-member with instructions about how to return their old laptop (whether outdated or broken).
All laptops must be returned **within 2 weeks of receiving the replacement laptop**, so please prioritize transferring information between laptops within this timeframe.

If an offboarded employee decides not to purchase and is not under a current litigation hold, then we will have them ship to our 3rd party vendor that handles sell backs, SellYourMac.
SYM will send them a shipping label, and in the US, a shipping box as well. **Please note shipping times may vary, expect between 2-4 weeks for Sell Your Mac to provide shipping information and packaging**

(If the IT department has record of a current litigation hold for the offboarded employee please consult with Legal before proceeding.)

All team-member laptops must be securely erased before being returned.
This not only protects the company, but also protects you since it is possible for personal information to exist on these machines.
Reformatting a computer is not sufficient in these cases because it is possible for sensitive data to be recovered after reinstalling an operating system.

## Other Resources

### Okta

In an effort to secure access to systems, GitLab is utilizing Okta.
The key goals are:

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorized connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and De-provisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.

To read more about Okta, please visit the [Okta](/handbook/business-ops/okta/) page of the handbook.

### Full Disk Encryption

To provide proof of Full Disk Encryption, please do the following depending on the system you are running.

- Apple : Take a screenshot showing both the confirmation of enabled Full Disk Encryption as well as the info showing your serial number.
    Both pieces of information can be found by clicking on the Apple icon in the top left corner of your screen.
    For proof of disk encryption, choose `System Preferences -> Security & Privacy`, and then choose the `FileVault` tab near the top of the window.
    For your serial number, choose the `About This Mac` option.
    Please get both pieces of information in a single screenshot.
- Linux : Take a screenshot showing the output of `sudo dmsetup ls && sudo dmidecode -s system-serial-number && cat /etc/fstab`

### Fleet Intelligence & Remote Lock/Wipe

GitLab has a large and ever-growing fleet of laptops, which IT Operations is responsible for maintaining.
In order to do this and combined with our Zero Trust security policies and various Compliance needs, there must be some measure of intelligence and reporting in place.
To accomplish this goal we are utilizing JAMF for MAC devices to obtain only the essential information required. For Linux machines we will be utilizing DriveStrike as a light-touch mechanism.

For more information regarding JAMF, refer to our [Endpoint Management](/handbook/business-ops/team-member-enablement/onboarding-access-requests/endpoint-management/) handbook page.

**DriveStrike**

DriveStrike is available for Linux operating systems, and also meets security needs for remote lock or wipe in emergencies.

DriveStrike installs for GitLab will NOT have the ability to execute remote commands.
DriveStrike installs for GitLab WILL have the ability to remotely lock or wipe hardware, for use in emergency or offboarding situations.
DriveStrike may be installed on non-GitLab hardware, opting-in to the same data collection and security.
DriveStrike collects the following information for GitLab:

- Usernames & Accounts present on the machine
- Machine Name, Make, Model, MAC, Serial #
- Hardware Specifications (CPU, RAM, HDD + % Used)
- Firewall status
- OS Version & Patch/Update status
- Disk encryption status
- Battery Health

This light-touch reporting allows us to meet business and compliance needs, while maintaining the privacy of the GitLab team member. This will remain a top consideration throughout the process.

**Drivestrike Installation Process**

If you are retaining your laptop/receiving a new laptop upgrade we will need to wipe your old laptop. The team member enablement team will email you a link with Drivestrike to install on the laptop that needs attention. To install follow the below workflow : -

1. Open the email on the device you want protected.
2. Click the link provided in the email to install
3. Use your `@gitlab.com` if installing DriveStrike prompts you for an email address.

### Slack Emoji Workflow

In an effort to improve effciency and promote self service while using Slack. Our team has creating an Emoji workflow in Slack that will send automated messages based on a specific emojis reaction.

The idea is that if a user posts a question in the IT-Help slack channel, an IT technician or anyone in Gitlab can react with an emoji and send a message that has helpful information.

All workflows will contain a button that a user can press to request help from a human. This is to show that we are not trying to degrade the employee experience.

#### How does it work?

The process is very simple:

1. Wait for a help request to come into the IT-Help Slack channel
2. Once a request (slack message) comes in, hover your mouse over it and press the Add reaction button.
3. Search for the appropriate emoji that corralates to the workflows shown at the table below.
4. Click the emoji. This will now add the emoji to the message and activate the workflow messages created (shown below)

Below is a table with the current workflows setup:

<table>
<thead>
  <tr>
    <th>Process</th>
    <th>App/Subject</th>
    <th>Emoji</th>
    <th>Message</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Gitlab 2FA Assistance</td>
    <td>Gitlab,dev, staging</td>
    <td>:gitlab_wizard:</td>
    <td>For 2FA related problems for your Gitlab account, please use your back up codes. If you saved these codes, you can use one of them to sign in.<br>To use a recovery code, enter your username/email and password on the GitLab sign-in page. When prompted for a two-factor code, enter the recovery code.<br>Once you use a recovery code, you cannot re-use it. You can still use the other recovery codes you saved. <br>For more information please visit this <a href="https://docs.gitlab.com/ee/user/profile/account/two_factor_authentication.html#generate-new-recovery-codes-using-ssh">page</a>.</td>
  </tr>
  <tr>
    <td>Okta 2FA Assistance</td>
    <td>Okta</td>
    <td>:okta:</td>
    <td>Hi {user]<br>Did you set up a YubiKey or Google Authenticator as another form of MFA? Use that to access your settings page to re-set your Okta Verify application.<br>If this was not helpful, an IT technician will reach out for assistance!</td>
  </tr>
  <tr>
    <td>Access Request</td>
    <td>Gitlab</td>
    <td>:access-request:</td>
    <td>Hello {user]<br>This request will require you to submit an access request <a href="https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=">here</a>. Please use the appropriate template (If no such template exists use the closest one and customize it)<br>After this request is submitted our team will action it as soon as possible.</td>
  </tr>
  <tr>
    <td>Laptop Repairs</td>
    <td>Laptops</td>
    <td>:laptop-repair:</td>
    <td>Hi {User}<br>In the event that your laptop is broken or damaged we will attempt to repair the machine. Can you please provide your serial number? This will allow us to check the machines warrenty<br>Replacement laptops for broken GitLab laptops can be purchased as needed by creating an <a href="https://gitlab.com/gitlab-com/business-ops/team-member-enablement/issue-tracker/-/issues/new?issuable_template=Laptop_Repair">issue</a> in the Team member Enablement issue tracker project.</td>
  </tr>
  <tr>
    <td>Laptop Troubleshooting</td>
    <td>Laptops</td>
    <td>:laptop:</td>
    <td>Hi user. We are sorry to hear that your laptop has started to run slow or you are experiencing freezing in apps. Follow this <a href="https://about.gitlab.com/handbook/business-ops/team-member-enablement/self-help-troubleshooting/#self-help-and-troubleshooting-tips" target="_blank" rel="noopener noreferrer">guide</a> and if you’re still having trouble, let us know! We’ll be monitoring this thread.<br></td>
  </tr>
  <tr>
    <td>Laptop Upgrade</td>
    <td>Laptops</td>
    <td>:new-laptop:</td>
    <td>Hello {user}<br>Team member Laptops can be refreshed after 3 years (of use!) without question. However, if the replacement laptop is outside the standardized specifications listed here than manager approval will be required before IT can purchase the replacement laptop. <br>If you feel you need a new laptop to be effective at your job before then, reach out to IT and your manager.<br>Please submit an upgrade issue <a href="https://gitlab.com/gitlab-com/business-ops/team-member-enablement/issue-tracker/-/issues/new?issuable_template=Laptop_Replacement" target="_blank" rel="noopener noreferrer">here</a>.</td>
  </tr>
</tbody>
</table>
