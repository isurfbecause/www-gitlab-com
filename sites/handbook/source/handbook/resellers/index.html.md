---
layout: handbook-page-toc
title: "Channel Partner Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

GitLab Channel Partners are the primary audience for this Handbook page.

### INTRODUCTION:  GitLab Partner Program Overview

The GitLab Partner Program is a new kind of program.  It’s open to everyone and like our overall company culture, everyone can contribute.  Although we welcome all partners, our program is structured to provide additional rewards for partners that make a commitment and investments in a deeper GitLab relationship.


The GitLab Partner Program enables partners and potential partners — including systems integrators, cloud platform partners, independent software vendors, managed service providers, resellers, distributors and ecosystem partners — to maximize customer value through the GitLab platform, and their value-added GitLab and DevOps services.  

### GitLab Value for Partners

GitLab is experiencing tremendous growth, which creates incredible opportunities for our partners.  Partners have the opportunity to create revenue growth for their companies through:

*   Product sales discounts or referral fees
*   Growing recurring revenues and renewals
*   Deployment and consulting services incentives and opportunities including implementation, integration, migration, education and process improvement services.


![alt_text](/images/gitlabvalueforpartners.png "image_tooltip")


### Why Become a GitLab Partner?

Companies need skilled, qualified experts to help them implement the latest DevOps best practices and technologies to improve the quality and speed of their software development. GitLab is the only vendor offering an end to end software development lifecycle solution.  This means that you can offer your customers the most complete solution on the market and help them develop efficient, integrated end to end processes.  The result?  Simplified development toolchains, faster delivery of quality software, and improved security and compliance.

And it’s not just the completeness of the solution that creates opportunities for you.  By joining the GitLab Partner Program, you will be partnering with a company that is growing annual recurring revenue by over 100%.  As our customer base grows, your business opportunities grow.  Additionally, you can help the thousands of users of the open source GitLab Community Edition that could benefit from an upgrade to the expanded features and functionality of the GitLab Enterprise Edition.  

Whether you are working with new customers, customer transitions from open-source to licensed editions, customer expansions or addressing other needs, your customers will need your help, creating a tremendous opportunity to expand your software sales and services businesses.  


#### Plan, Build and Grow Your GitLab Practice

Collaborate with GitLab to build a well-planned, go-to-market approach to success. With co-marketing, co-selling and service delivery,  we provide the tools to successfully grow your customer base and increase profits.  


#### Deliver Valuable Deployment and Integration Services

GitLab offers a wide range of meaningful enablement and support programs – including training and certifications.  Help customers get started on their journey with GitLab by offering deployment and integration services.  Utilize your GitLab skills to expand your business with companies of all sizes, industries and geographies.  


#### Develop Specialized, In-Depth Expertise

Your company has unique expertise and best practices in helping companies optimize their software development systems and processes.  Our goal is to help you benefit from your know-how as you help companies deliver software more efficiently, with higher quality and security.


### GitLab Investment in your Success

The GitLab Partner Program makes it  easier for partners  to maximize the value of your DevOps expertise and the GitLab platform.  To unlock that value, GitLab is making significant investments across our partner lifecycle, including:



*   Training and Enablement
    *   Sales training and enablement, ensuring that our partners are as well trained and equipped as our own sales teams.
    *   Technical services, training and enablement designed to help you drive customer usage and adoption
    *   GitLab Badging, Accreditation & Certification.
*   Global expansion of our channel team to support the growth of your GitLab success.
*   Improved ease of doing business, via a new partner portal with online resources and tools.
*   Marketing resources and marketing development funds for lead creation.
*   Support for our partners throughout the entire customer journey.


### Partner Program Tracks

The GitLab Partner Program consists of three tracks to support the different ways our partners go to market:

*   **Open** - Resellers, integrators and other sales and services partners join the program in the Open track.  Open is for all Partners in DevOps space, or ItaaS and other adjacent spaces that are committed to investing in their DevOps practice buildout. Also the Open track is for Partners seeking to develop customers or just want to learn about Gitlab and participate in the Gitlab partner community.  GitLab Open Partners may or may not be transacting partners, and can earn products discounts or referral fees.
*   **Select** partners are by invitation only and are reserved for partners that make a greater investment in GitLab expertise, develop services practices around GitLab and are expected to drive greater GitLab product recurring revenues.  
*   **Technology** partners that drive GitLab solutions through product integrations and marketplaces. Technology Partners are key to completing our customer solutions through integrations with Cloud platforms, legacy DevOps solutions and other solutions needed to meet customer requirements.

### Getting Started with GitLab

To engage with GitLab, partners start by completing the online registration process on about.gitlab.com/reseller page, executing the GitLab Partner Agreement and completing the partner onboarding steps.

Once a partner contract is executed , you will have access to the GitLab Partner Portal and other members of your team will be able to register and be associated with your account.  At that time, you will be able to enter deal registrations and referral transactions.  You will also have access to sales tools, technical resources, and training courses.  

#### Program Requirements and Benefits

|    |**GitLab Open** | **GitLab Select (Invitation only)** | **GitLab Technology** |
|-------------------|:---------------------------------:|:-------------------:|:---:|
| Overall Program Requirements |  |  |  |
| GitLab Partner Agreement | X | X | X |
| # of Sales Certified resources | 2 | 5 |  |
| # of Technical Certified resources | 1 | 3 |  |
| Exec Sponsored Joint Business Plan |  | X |  |
| Program Rev Targets Min - BusPlan Committed |  | $300K |  |
|  |  |  |  |
| Sales Benefits |  |  |  |
| Deal Registration Discounts | X | X |  |
| Referral Fee | X | X | X |
| Renewals Discounts | X | X |  |
|  |  |  |  |
| Support Benefits |  |  |  |
| Partner Helpdesk | X | X |  |
| Level 2 Support Hotline |  | X |  |
| Dedicated Channel Manager |  | X |  |
| Free Limited Instance of GitLab Ultimate/Gold - 5 users/6mo |  | X |  |
|  |  |  |  |
| Marketing |  |  |  |
| Partner Portal | X | X | X |
| Proposal Based MDF | X | X |  |
| Sales & Pre-Sales Technical Enablement | X | X | X |
| Program Logo | X | X | X |
| Program & Certification Badges | X | X |  |
| GitLab.com Listing | X | X (featured) | X |
| Demand Generation Resources | X | X | X |
| Leads |  | X |  |
|  |  |  |  |
| Separate Partner Tracks |  |  |  |
| Services |  |  |  |
| Packaged Professional Services Discounts | X | X |  |
| Product Services Certification |  | X |  |
| Operational Services Certification |  | X |  |
| Customer Success Certifications |  | X |  |
|  |  |  |  |
| Managed Services |  |  |  |
| Managed Services certification |  | X |  |
| Managed Services Support Discount |  | X |  |


## B. Partner Program Guide

GitLab’s Channel Partner Program offers market-leading training, tools, and support to help accelerate sales opportunities and grow your business with GitLab.

This Partner Program Guide will provide you with details on the rules of engagement around the program, how to complete specific tasks as you work with GitLab, and highlight key GitLab resources available to you.  In it you will find:



*   Section 1:  Program Requirements, Benefits and Guidelines
*   Section 2:  Working with GitLab
*   Section 3:  Tools and Resources


### SECTION 1: Program Requirements, Benefits and Guidelines

In this section, we cover:

*   Partner Program Tracks
*   Program Requirements and Benefits
*   Training and Certification Requirements and Benefits
*   Discounts and Referral Fees – Definitions and Qualifications
*   Deal Registration Program Overview
*   Marketing Development Funds (MDF) Program
*   Developing a GitLab Services Practice


###  Partner Program Tracks

The GitLab Partner Program consists of three tracks that support the different ways our partners go to market:

1. **Open** - OResellers, integrators and other sales and services partners join the program in the Open track.  Open is for all Partners in DevOps space, or ItaaS and other adjacent spaces that are committed to investing in their DevOps practice buildout. Also the Open track is for Partners seeking to develop customers or just want to learn about Gitlab and participate in the Gitlab partner community.  GitLab Open Partners may or may not be transacting partners, and can earn products discounts or referral fees. We provide:
  *   Broad Sales and Services Enablement
  *   Discounts and Referral Fees
  *   GitLab Badges
  *   Field Support
2. **Select** partners make a greater investment in expertise, develop services practices around GitLab and are expected to drive greater product recurring revenues. Participation in the Select Partner track is by invitation only. We provide:
  *   Focused Strategic Relationships
  *   Discounts and Referral Fees
  *   Targeted Sales and Services Enablement
  *   Badges and Certifications
  *   Access to Proposal-based MDF
  *   Ultimate/Gold-Level Support
  *   Business Planning and QBRs
  *   Featured in the GitLab Partner Locator
Since participation in the Select Track is by invitation only, partners are not automatically upgraded. To be considered, they must meet these minimum requirements:
  *   Five (5) Sales Certifications in the region specified in your GitLab Partner Contract
  *   Three (3) Technical Sales Certifications in the region specified in your GitLab Partner Contract
  *   Executive-sponsored joint business plan
  *   $300k minimum program revenue, defined as Incremental Annual Customer Value (IACV).
  *   Dedicated DevOps sales and/or services practice
  *   Minimum one GitLab demand generation activity per quarter
3. **Technology** partners drive GitLab solutions through product integrations and marketplaces as part of our Technology Alliances program. They are key to completing our customer solutions through integrations with cloud platforms, legacy DevOps solutions and other solutions needed to meet customer requirements.

    For additional information about our Technology Alliances program, please visit the[ Alliances Handbook Page.](/handbook/alliances/)



### Program Requirements and Benefits

|   |**GitLab Open** | **GitLab Select** <br> **(Invitation only)** | **GitLab Technology** |
|------------------|:---------------------------:|:-------------------:|:---:|
| Overall Program Requirements |  |  |  |
| GitLab Partner Agreement | X | X | X |
| # of Sales Certified resources | 2 | 5 |  |
| # of Technical Certified resources | 1 | 3 |  |
| Exec Sponsored Joint Business Plan |  | X |  |
| Program Rev Targets Min - BusPlan Committed |  | $300K |  |
|  |  |  |  |
| Sales Benefits |  |  |  |
| Deal Registration Discounts | X | X |  |
| Referral Fee | X | X | X |
| Renewals Discounts | X | X |  |
|  |  |  |  |
| Support Benefits |  |  |  |
| Partner Helpdesk | X | X |  |
| Level 2 Support Hotline |  | X |  |
| Dedicated Channel Manager |  | X |  |
| Free Limited Instance of GitLab Ultimate/Gold - 5 users/6mo |  | X |  |
|  |  |  |  |
| Marketing |  |  |  |
| Partner Portal | X | X | X |
| Proposal Based MDF | X | X |  |
| Sales & Pre-Sales Technical Enablement | X | X | X |
| Program Logo | X | X | X |
| Program & Certification Badges | X | X |  |
| GitLab.com Listing | X | X (featured) | X |
| Demand Generation Resources | X | X | X |
| Leads |  | X |  |
|  |  |  |  |
| Separate Partner Tracks |  |  |  |
| Services |  |  |  |
| Packaged Professional Services Discounts | X | X |  |
| Product Services Certification |  | X |  |
| Operational Services Certification |  | X |  |
| Customer Success Certifications |  | X |  |
|  |  |  |  |
| Managed Services |  |  |  |
| Managed Services certification |  | X |  |
| Managed Services Support Discount |  | X |  |

### Training and Certification Program and Requirements

GitLab has developed core training and certification for sales, solution architects (pre-sales engineers), and post sales engineers and consultants.  The GitLab certification program is designed to provide our partners with similar training as GitLab employees themselves.  GitLab is announcing three certifications for the GitLab Partner Program:

1. **GitLab Sales Core** - This is the basic certification for sales and the prerequisite for pre-sales technical professionals.  The curriculum provides an overview of the market GitLab services, customer personas and needs, GitLab solutions and positioning of GitLab.  By completing the GitLab Sales Core, you’ll earn a GitLab Core Sales accreditation and meet the program requirement for Sales certification.
2. **GitLab Solution Architect Core** - This is the basic certification for pre-sales technical professionals and provides a deeper understanding of demonstrating, deploying, integrating and optimizing GitLab solutions.  This certification is a mix of online learning and hands-on labs.  GitLab Sales Core is the prerequisite for GitLab Solution Architect Core.  GitLab Solution Architect Core meets the program requirement for Pre-sales technical certification.  _NOTE:  The hand-on lab components for this certification are not yet available for partners.  Partners will be notified once it is available._
3. **GitLab Technical Core** - GitLab Technical Core is the foundational technical training for post-sales technical professionals and can also be a valuable training for pre-sales technical professionals.  As we continue to develop our services enablement offerings, partners will need to develop a custom plan with their Channel Account Managers to meet the certification requirements.

 On-demand, online training courses are available on the GitLab Partner Portal.  Registration for courses delivered to a live audience will also be available on the Partner Portal. Sales Core training takes approximately 3.5 hours to complete, Solution Architect training takes approximately 5 hours to complete. 

Upon completion of each certification program, partners will earn a badge and will address the corresponding company certification requirement.


### Discounts and Referral Fees

The GitLab Partner Program helps develop your practice to best fit your business model. You can earn one-time and recurring revenues from product and services sales, referrals, and services delivery and resale.

To view the GitLab Partner Program Discount and Referral Fee Table please visit the [GitLab Partner Portal](https://partners.gitlab.com) (must be an authorized Partner) and access discount table in the Program Documents folder in the Asset Library.  Note, no discounts are given on sales of Bronze / Starter sales or renewals.

Authorized Public Sector Partner can earn one-time and recurring revenues from product and services sales, referrals, as well as services deliver and resale.  To view Public Sector Discounts please visit the [GitLab Partner Portal](https://partners.gitlab.com) (must be an authorized Public Sector Partner) and access discount table in the Program Documents folder in the Asset Library.


GitLab employees can access the discount table [here.](https://gitlab.my.salesforce.com/0694M000008xAk4)

#### Definitions and Qualifications


*   **PIO Discount – **You can earn the largest available product discount with a **Partner Initiated Opportunity** . It can be a new opportunity to our sales team or one that was significantly impacted by the partner, early in the sales cycle.  It can be for a new or existing customer.  True ups and add-on licenses at renewal can also qualify for PIO discounts. The partner is expected to assist the GitLab Sales team in closing the sale.  
This is an upfront discount and the amount depends on your track within the GitLab Partner Program. To qualify for PIO discounts, partners must submit a deal registration to GitLab via our Partner Portal, and it must be approved by GitLab Sales to qualify. PIO is available for both resale and referral opportunities.  Please visit the Deal Registration Program guideline in the Program Guide for additional details.
*   **Partner Assist Opportunity** is a discount earned for a GitLab-sourced opportunity where the partner assists our sales team in closing the deal. This may include demonstrating GitLab software, organizing executive meetings, delivery of services, etc. This is an upfront discount and the amount depends on your track within the GitLab Partner Program. To qualify for Partner Assist discounts, partners must submit a deal registration to GitLab via our Partner Portal, and it must be approved by GitLab Sales to qualify. Please visit the Deal Registration Program guideline in the Program Guide for additional details.


*   **Partner Fulfillment Opportunity** is fulfilled by a partner but closed independently by the GitLab sales team. The partner only processes the order and doesn’t provide additional support to close the deal.


*   **Services Attach Discount and Rebate** is an incentive for partner-delivered services provided to the end user related to their use of the GitLab Software (Core/Free and Starter/Bronze sales do not qualify). Services include product-related services (such as deployment, integration and migration), operational and strategic. For resale partners, the first Services Attach incentive is administered as an upfront 2.5% discount from the GitLab license price on the initial sale. Partners that do not resell or resellers that sell their own services after the initial sale may earn a rebate of 2.5% of the net license price of the most recent software sale for a period of up to six (6) months.
    *   This incentive is stackable to up to three (3) independent services engagements provided by the partner to a single end user. The maximum is 7.5% discount and/or rebate with any combination of upfront or post sales of partner-delivered services.
    *   Partner Services engagements must meet the following partner services deal size minimums:
        *    1st deal: => $7,500
        *    2nd deal: =>$10,000
        *   3rd deal: => $10,000

    Services Attach requests shall be submitted to GitLab via the Partner Portal as a deal registration, Services Attach requests shall be submitted to GitLab via the Partner Portal as a deal registration (Deal Registration Type = Services Attach), must include reasonable information and documentation (as requested by GitLab) to demonstrate the Referral Clients receipt of the Partner Professional Services and must be approved by GitLab Sales.

    Each rebate paid out quarterly, 45 days after the end of each quarter.  


*   **Referral Fees** - Referral fees are paid to partners for identifying Partner Initiated GitLab software sales opportunities that are not being resold by the partner. To qualify for a referral fee, partners must enter a referral fee deal registration in our Partner Portal. Each registration received by GitLab qualifies as a "Qualified Referral," provided that the referral is a GitLab sales opportunity that is new to GitLab, and the customer/prospect is willing to enter into a binding written agreement with GitLab to use our products and services.
    *   All referral deal registrations must be approved by GitLab before becoming eligible for a referral fee. Referral fees are paid out quarterly, 45 days after the end of each quarter.
*   **Services Resale** - Any partner-sold services delivered by the GitLab Professional Services team. Partners qualify for a program discount on services resale if the services are included on the order of a deal registered opportunity.
*   **Subscription Renewals** - At the end of their GitLab license subscription, customers must renew to continue their subscription. For customers whose most recent GitLab purchase was through a reseller partner, the incumbent reseller can earn a discount on the resale based on their program track, unless 1) the incumbent partner is no longer in compliance or 2) the customer provides a written request to GitLab to renew with a different partner.  A deal registration on the original opportunity is not required for a partner to earn incumbent status.

    In some cases, a customer purchased their most recent subscription directly from GitLab, but is directed to a partner for renewal.  GitLab has a significant install base of these customers with a high propensity for up/cross-sell and services opportunities. Partners are encouraged to engage with these customers but for now, the first renewal of a formerly direct customer will not be discounted for partners.  However, these new customers will create additional opportunities for growth through:  

    *   Upsell and expansion opportunities
    *   New integration, operational and other professional services opportunities
    *   The sale and integration of GitLab technology partner solutions
    *   Add-on sales of other non-GitLab products
    *   Subscription renewal discounts on future renewals

 ** **


### The Deal Registration Program Overview

To promote the growth and success of our channels, to manage channel conflict and provide a systematic approach to pursuing sales opportunities, GitLab has formalized our rules of engagement and deal registration procedures.

Only a properly completed Deal Registration Form can initiate an engagement. It is therefore in all parties’ best interest that you complete a Deal Registration Form as soon as a qualified sales opportunity is identified.  Since the first qualified deal registration is approved, partners are encouraged to submit their deal registrations as early as possible to lock in the biggest discount.

Deal registration and associated discounts are only available to partners that have signed the GitLab Partner Program Agreement, and for deals approved by GitLab Sales. In the GitLab Partner Program, you need to register individual sales opportunities. Partners may not register an account or combine opportunities.


*   Deal registration approval is based upon order of receipt of the registration, qualification of the opportunity, partner ability to deliver in-country/region support, partner relationship with customer.  Final deal registration approval decision will be made by GitLab Sales.
*   Only one partner can earn a deal registration discount per opportunity.  Partners, other than the partner granted the deal registration discount that request a quote, will receive the fulfillment
discount rate. 
*   New customer opportunities or new opportunities with existing customers can qualify for deal registration.  Add-on sales to renewals can qualify for deal registration.
*   Approved deal registrations have standard 90-day expiration from the date of original approval (Deal Registration extensions beyond the initial 90 days approval are at the sole discretion of GitLab).
*   GitLab collaborates with partners holding the approved deal registration and is available to support partners throughout the entire sales process.
*   In the event the engagement is dissolved, the GitLab Sales Rep will generally notify you by phone or email. GitLab will reconsider other deal registrations submitted for this deal, in chronological order of submission. If there are no other registration requests submitted, the GitLab Sales Rep will typically initiate engagement with a reseller of the GitLab Sales Rep’s choosing.

To learn how to enter a deal registration, vist the [Deal Registration Instructions](/handbook/resellers/#deal-registration-instructions) section below.

###NFR Program/Policy 


The GitLab NFR (Not-for-Resale) Program offers qualified GitLab partners access to our solutions at no cost to our partners. The NFR program provides partners with an opportunity to develop greater expertise on the GitLab platform. We encourage all GitLab Partners to participate in the program and set up GitLab solutions. Consider using your environment to create integrations with GitLab Alliance solution partners.  This way your staff can fully understand the benefits and features of the GitLab products and be better prepared to demonstrate the products to customers.



*   The NFR software may only be used for internal training, integration testing and customer demonstrations. 
*   The software may not be used in customer or potential customer environments, or for day-to-day operational use. 
*   Partners will receive NFR Licenses at the Ultimate level for a 12 month period
    *   Select partners can request up to 25 licenses 
    *   Open partners can request up to 10 licenses  
*   Please allow 2 business days for processing your request. 

    **NFR Program Eligibility **


    To be eligible for the NFR program, partners must:

*   be Open or Select partners in good standing
*   have at least one employee that has completed the Solution Architect Certification or Professional Services Engineer Certification training, lab and exam (with a passing score).

    **NFR Renewals **


    GitLab NFR licenses expire after a 12 month subscription period, unless they are add-on licenses, in which case they expire with the partner’s oldest NFR licenses.  For partners in good standing, the subscription will be automatically renewed.


    **Support **


    [Support ](https://about.gitlab.com/support/)will be provided by the GitLab support team. GitLab Solution Architects may also be available to help partners build out training, testing and demo labs.


    **NFR Program Terms and Conditions: **

1. NFR software and services may be used solely and exclusively by the partner for the following purposes: 
    1. Internal employee training
    2. Integration testing with related devops products and platforms, and 
    3. Partner led product demonstrations to prospective customers.
2. Partner in-house production use for customer engagements or internal development efforts requires purchased GitLab licenses which are available to partners at a discount.  Use of the NFR licenses in a customer environment, including for managed services is strictly prohibited. GitLab channel partners in compliance with the GitLab partner program are eligible for the NFR program, but they must have at least one employee that has completed the Solution Architect Certification or Professional Services Engineer Certification training, lab and exam (with a passing score).
3. Select partners may request up to 25 licenses and Open partners up to 10 licenses without additional approval.
4. All software purchase under the NFR Program are subject to the terms and conditions of the GitLab end user license agreement at [https://about.gitlab.com/terms/](https://about.gitlab.com/terms/) 
5. GitLab reserves the right to audit the use of NFR licenses to ensure they are in compliance with the NFR program, and reduce the number of licenses to a partner if they are not in compliance with the program.
6. GitLab reserves the right to change or cancel the NFR Program at any time and for any or no reason.

### The Marketing Development Funds (MDF) Program

GitLab Open and Select partners have access to the proposal-based GitLab Marketing Development Funds (MDF) Program, which provides funding support for eligible marketing and partner enablement activities that help to:

*  Acquire new GitLab customers
*  Drive demand within an existing mutual customer base
*  Help partners educate their GitLab teams

* **To receive MDF, you must:**
  *  Submit a proposal to GitLab via the GitLab Partner Portal
  *  Have an authorized GitLab Marketing Manager review and approve the proposal
  *  Promptly submit the required proof of performance documentation to GitLab 30 days following the activity


##### MDF Investment Objectives



1. Lead/Demand Generation
    *   The objective of these activities would be to gain net new end-user customers for you and GitLab. Examples of eligible activities include seminars, workshops, telemarketing or email marketing campaigns, participation in trade shows, exhibitions and direct mail.
    *   Eligible costs may include mailing costs, fulfillment items, lead qualification, distribution, venue costs, food and beverages (excluding alcoholic beverages), speakers’ fees, management costs and lead capture.
2. Partner Enablement
    *   Enablement activities are events with the objective of educating partner teams about GitLab’s market, customers and solutions. Examples include local training events, regional sales meetings and partner sales kick-offs.
    *   Eligible costs may include fulfillment items, venue costs, food and beverages (excluding alcoholic beverages) and sponsorships.
    *   Detailed examples of eligible costs are included in this document’s “Proof of Performance (POP)” section


##### Eligible Activities and Required Proof of Performance MDF Activities

Proof of performance includes third-party invoices AND at least one of the following activities that shows GitLab or partner representation. For activities that require a lead list, you must submit it as part of your proof of performance as well.


| **Eligible Activity** | **Description** | **Proof of Performance** |
|--------------------|---------------------------|-------------------------------------------------------------|
| **Collateral Sales Tools** | Printed or web-based material developed to assist in your efforts to promote GitLab solutions (product briefs, data sheets, case studies, etc.). The document must include the GitLab logo. | • Electronic version or hard copy of marketing collateral <br> • Graphic files or digital photos that show the integration of GitLab brand/solutions <br> • eDM/Direct mail |
| Events | Any industry or partner event that you plan to attend to promote or host GitLab solutions. This can be done in collaboration with other vendors that don’t compete with GitLab. Events may also include enablement activities. | • Electronic version or hard copy of invitation <br> • Marketing collateral• Photo of display/booth/event <br> • Lead list/Attendee list (to include: Company Name, Contact, Title, Email, Phone, Full Address), Funded Head (Distribution Only) |
| Partner Sponsorship | Participation in a partner-led event or activity or third-party event. | • Contract of signed sponsorship |
| Partner Training | In-person or virtual events focused on enablement, sales or technical training. | • Certificate of completion• Group class – attendee list REQUIRED (to include: Company Name, Contact, Title, Email, Phone, Full Address)• Catering/Facilities invoices (if applicable) |
| MarketingMaterials | The delivery of co-branded marketing materials (digital or physical) to promote GitLab solutions. Eligible assets include emails, direct mailers and social media posts, and must include the GitLab logo. | • Electronic version or hard copy sample of direct mail, newsletter• Lead list (to include Company Name, Contact, Title, Email, Phone, Full Address) |
| Lead Generation/ Telemarketing | Delivery of co-branded messages via telephone or other mediums to gain net-new customers. The message should focus on GitLab solutions. The contact list should include Company Name, First Name, Last Name, Title, Full Address, Email, Phone. | • Telemarketing script of how GitLab brand or solution were incorporated into the campaign• Itemized call record or campaign metrics• Lead list or call list (to include: Company Name, Contact, Title, Email, Phone, Full Address)• Internal invoice accepted |
| Webcast/Webinar | A syndicated or on-demand web event with the purpose of either lead generation or continuing education. This can be conducted in collaboration with GitLab or GitLab-approved third-party vendors. | • URL to recorded online event, podcast, or presentation that shows integration of GitLab brand and solutions• Lead list/Attendee list (to include: Company Name, Contact, Title, Email, Phone, Full Address)• Invitation to the online event/podcast• Follow up/additional activities |
| Sales Incentive | Awards or financial incentives provided to sales or technical team for successfully selling GitLab solutions. These can be SPIFFs, bonuses, vouchers, prizes, etc. (but not cash). | • List of qualified accounts and contact details as well as the account manager name (lead list)• Receipts for gifts purchased• Incentive program promotion/flyer• List of awardees and campaign report (to show what has been achieved) |
| Funded Head (By Invitation Only) | Headcount dedicated to sell or develop GitLab solutions. These include sales resources (such as sales engineers). Each funded head project needs an approved business plan and request form that describes objectives of the role and time frame. | • Contract between GitLab and your partner business plan• Quarterly objectives and results |
| Promotional Items (Distribution Only) | Merchandise, items or gifts imprinted with the GitLab logo. | • REQUIRED one of the following – Digital photos or graphic files of promotional items showing integration of GitLab logo, brand solutions |


#### Marketing Efforts

You are asked to use reasonable efforts to promote and market the GitLab products and services to prospects within your territory.


*   You conduct business in a manner that reflects favorably at all times on GitLab products and services and the good name, goodwill and reputation of GitLab. You may not engage in any deceptive, misleading, illegal or unethical practices that you realize would be detrimental to GitLab, GitLab products and services or the public, including but not limited to, disparagement of GitLab, its products and services, and make no false or misleading statements or representations with respect to itself, GitLab, or GitLab products and services.
*   You are authorized to use GitLab’s names, marks, logos, and other identifiers as approved in advance in writing, on or in connection with your own marketing and promotion of GitLab products and services. Your use of GitLab logos needs to be solely in accordance with the GitLab Partner Program Agreement and any additional trademark guidelines we provide.



#### Developing a GitLab Services Practice

Professional Services are a cornerstone of the GitLab Partner Program.  Professional services provide partners a major source of revenue as they help partners deploy, integrate and optimize their GitLab solutions.  

Whether you are working with new customers, transitioning from open-source to licensed editions, proposing customer expansions or addressing other needs, your customers will need your help, creating a tremendous opportunity to expand your software sales and services businesses.  As part of your services practice, you will grow your business when you:


##### Deliver Valuable Deployment and Integration Services

GitLab offers a wide range of meaningful enablement and support programs – including training and certifications.  Help customers get started on their journey with GitLab by offering deployment and integration services.  Utilize your GitLab skills to expand your business with companies of all sizes, industries and geographies.  

*   Implementation Services - Optimize your customer’s adoption of GitLab through implementation services.
*   Migration Services - Facilitate your customer’s transition to GitLab through migration services.  These opportunities grow as customers adopt more of the end to end GitLab solutions
*    Integration Services - Integrate with the services and solutions your customer’s team needs to be productive and to complete their DevOps solution.

##### Develop Specialized, In-Depth Expertise
Develop Specialized, In-Depth Expertise


Your company has unique expertise and best practices in helping companies optimize their software development systems and processes.  Our goal is to help you benefit from your know-how as you help companies deliver software more efficiently, with higher quality and security.

*   Optimization Services - help your customers optimize their software development infrastructure and processes through valuable consulting services
*   Education Services - Your product specialists - technology professionals themselves - can train customers to become proficient in GitLab quickly.
*   Specialized Training - GitLab enables all stages of the DevOps lifecycle, and you provide specialized training such as CI/CD, version control, metrics and more to help your customers expand their adoption of GitLab

 As part of our GitLab Partner Program, we provide resources, training and other investments to help you build out your services practice.  Your Channel Account Manager will help you develop a plan and utilize GitLab resources to build your GitLab services business.


### SECTION 2: Working with GitLab

Onboarding our partners is incredibly important to us at GitLab. We want to ensure that you quickly get the training and resources you need to get off to a strong start with GitLab.

In this section, we cover:

*   Getting Started with GitLab – Partner Registration
*   Deal Registration – Instructions
*   Quote to Order
*   Training and Certifications
*   Marketing and Support Services
*   Requesting Marketing Development Funds (MDF)
*   Tools and Resources


#### Getting Started with GitLab

To get started with GitLab,  register your company by going to https://partners.gitlab.com. The system will prompt you for your company information as well as your personal contact information.  A GitLab Channel Account Manager will contact you for qualification.  If you agree to proceed, your company will be required to sign a GitLab Reseller agreement.  Once you are registered, you should:

*   Work with to your GitLab Channel Account Manager to begin business and sales planning.
*   Visit the portal and encourage your colleagues to sign up at https://partners.gitlab.com. 
*   Take the Sales training as well as other relevant learning.
*   Register your first deal.


#### Completing Training and Certifications

GitLab training programs are available primarily through the [GitLab Partner Portal](https://partners.gitlab.com).  There you will find the certification courses and testing needed to meet your GitLab program requirements for sales, pre-sales technical and professional services training.  Additional training materials can be found in the GitLab Handbook:


*   [Sales Quickstart Learning](/handbook/sales/onboarding/sales-learning-path/) - This is an overview of the GitLab Sales training plus additional learning materials.
*   [Reseller Webcast Archive](https://about.gitlab.com/webcast/reseller/).  Review recent channel webcasts providing deep dive learning on key GitLab topics.
*   [GitLab Training Handbook page](https://about.gitlab.com/training/) - This page provides a subset of on-demand product and technical content suggested based on your role and skill level.
*   [GitLab Partner Bootcamp](/handbook/resellers/bootcamp) - Self paced, hands-on training for consultants and technical personnel


#### Deal Registration Instructions

Our GitLab Deal Registration process allows us to incentivize our partners to source and close GitLab products and services opportunities and avoid channel conflict. You can find an overview of the deal registration program, including rules of engagement [here](/handbook/resellers/#the-deal-registration-program-overview).  The following provides instructions for this process.


##### Submitting a Deal Registration


###### Basic information

1. Go to the portal at [partners.gitlab.com](https://partners.gitlab.com/) and select Register a Deal.
2. Select type of deal registration.
3. Complete information. Ensure information is complete. GitLab will not engage based on a speculative or poorly defined opportunity, or with a reseller who is not eligible to win a sales opportunity.
    1. For Services Attach, partners must include proof of services delivered to the customer.



![alt_text](/images/Channel-Handbook1.png "image_tooltip")


4. Submit the deal.
5. Go through the review and approval process.


##### As deal registrations are received, GitLab Sales Reps:


*   Review deal registration applications
*   Advise if you are the first to register an opportunity
*   Work with you to confirm whether a deal is a qualified sales opportunity

##### How it works:

*   Only a properly completed deal registration submission can initiate an engagement and, as mentioned, engagements are not exclusive. Thus, it is in all parties’ best interest to complete a deal registration form as soon as a qualified sales opportunity is identified.
*   The GitLab Sales Rep checks to see if GitLab is already engaged with another reseller on this opportunity. If so, the Sales Rep rejects the deal registration and notifies you that we cannot engage with you.
*   If GitLab is not already engaged with another reseller, the Sales Rep or Account Executive contacts you to schedule a meeting or conference call with the customer to confirm that the following requirements are met:
    *   The information submitted on the deal registration form is complete and correct.
    *   A qualified sales opportunity exists.
    *   You are in full compliance with an appropriate and effective GitLab reseller agreement and all applicable policies/programs.
    *   The customer has not selected an alternate GitLab reseller for this deal.
    *   The customer agrees that you are eligible to win this business.
    *   Note: “Eligible” means that you meet all contractual or regulatory requirements to bid on the deal and that the customer is willing to buy you. This is particularly relevant with government contracts/bids, but could also apply to commercial work.

If any of the above requirements are not met—or you fail to promptly schedule a meeting or conference call with the customer—the GitLab Sales Rep may advise you that we cannot engage with you for this opportunity. GitLab will then reconsider other deal registration applications submitted for this deal, in order of submission. You may subsequently attempt to remedy this situation and submit another deal registration form for this deal, but this will be considered a new submission.


#### Referral Fees and Discount Scenarios*
*scenario percentages are for example only and may not reflect your actual discount

| **Scenario #1** <br> - GitLab Finds Lead - Partner Fulfills | - 2% Discount of IACV |
|----------|--------------------------------------------------------------|
| **Scenario #2** <br> - GitLab finds Premium Customer <br> - Requires OPEN Channel Assist <br> - Partner delivers QuickStart | 10% Discount or Referral Fee of IACV <br> - 2.5% Extra Discount or Referral for delivering services |
| **Scenario #3** <br> SELECT Partner finds Ultimate Customer <br> - Sells GitLab Education <br> - Delivers QuickStart <br> - Delivers DevOps Assessment (6 mo. later) | 20% Discount of IACV <br> - 10% Discount of GitLab Education Service <br> - 2.5% Discount of IACV (Service Attach/QuickStart) <br> - 2.5% Discount of IACV (Service Attach/QuickStart) |
| **Scenario #4** <br> - OPEN Partner Finds Premium or Ultimate Customer |  <br> - 10% Discount or Referral Fee of IACV |


#### Quote to Order


##### Receive a GitLab Reseller Quote

Attached below is a sample quote. When you request a quote, you receive a document that looks very similar to this. This quote reflects your reseller price exclusive of incentive bonuses, and is not meant for the end customer. You will need to generate your own quote to the end customer.

The quote comes with a[ Sertifi](http://corp.sertifi.com/) link. E-signing the quote with Sertifi initiates the invoicing process and causes our systems to invoice you. Do not e-sign the quote until you are ready to be invoiced. Do not e-sign the quote if your customer will be paying us directly.

Note that we do not generate a quote, or fulfill an order, without an end user contact complete with email, shipping address, and postal code.

![alt_text](/images/Channel-Handbook2.png "image_tooltip")


#### License Key Delivery

Once the order is invoiced and the customer accepts the EULA, they are able to download their GitLab license key. Note that you do not receive a license key - it goes directly to the customer.


#### Remitting payment to GitLab

You can arrange for payment either via invoice, or your customer can pay us directly.


##### By Purchase Order

If your customer will be paying us via a Purchase Order, then you must email us a copy so that we may invoice the customer. Please send the Purchase Order to POfulfillment@gitlab.com for fulfillment.


##### Paying via Invoice

To pay via invoice, simply e-sign the[ reseller quote](/handbook/resellers/#gitlab-quote) to initiate an invoice, then remit payment in USD to the bank listed on your quote.


##### Execute the GitLab EULA

All orders require an executed[ EULA](https://about.gitlab.com/terms/signature.html) when there's a new subscription or an add-on. There are 2 methods of obtaining a EULA:



1. **License Key Deployment:** The default for reseller orders is that the end customer will receive a link to download their license key. The customer needs to click an acceptance of terms to get their key.
2. **Physical Signature:** Some customers may require a fully countersigned document.

An order is not complete without a signed agreement.


#### Evaluation Licenses for Prospects


*   We will issue a 30-day evaluation license for your prospects if the deal is properly[ registered](/handbook/resellers/#deal-registration).
*   We can renew this license if your customer needs more time.
*   Upon the second request for renewal (the third license), we assist you with a managed evaluation, where goals are set for the customer to meet. One of our Solutions Architects works with you and the prospect to bring them to completion before the third evaluation expires.


#### Requesting a GitLab NRF (Not-for-Resale) License  


1. Authorized GitLab Partner fills out the NFR License Request Form which can be found on the [GitLab Partner Portal](https://partners.gitlab.com/English/) on the Services Page. 
    1. Partner Help desk (PHD) reviews the request and works with the partner if additional information is needed.
    2. Once activated the requestor will receive a license key via email. 


#### Marketing Resources

GitLab marketing resources can be found on the [GitLab Partner Portal](https://partners.gitlab.com) as well as in the GitLab Handbook.  VIsit the portal or select the links below to the corresponding resources and instructions for usage.  



##### GitLab Branding Usage

*   [GitLab boilerplate web content](https://docs.google.com/document/d/1jzC2l88sKPDUWSXEgae4tqAg_QR34RDl6mPN5V8a0Mw/edit?usp=sharing)
*   [GitLab official logo, guidelines and other artwork](https://about.gitlab.com/press/) - Scroll all the way down to the _Images & Logos_ section

##### Campaigns on portal

*   Learn more about our campaigns:[ /handbook/marketing/product-marketing/analyst-relations/channel-marketing/](/handbook/marketing/product-marketing/analyst-relations/channel-marketing/)

##### Swag Requests

*   Please submit your application for GitLab branded swag[ here](https://docs.google.com/forms/d/1x2qP8EyEu2Y_XmIt7txudUYh-PP_Tst6hRuNq3a7Ruc/edit?usp=sharing). We have been known to co-fund co-branded or locally produced SWAG. Submit your idea[ here](https://gitlab.com/gitlab-com/resellers/issues/new) and choose the reseller template.


#### GitLab Landing Page Guidelines

Your website needs to have a landing page with information about GitLab. You can see what others have done from the[ Resellers page](https://about.gitlab.com/resellers).


*   We prefer your landing page to be at yoururl.com/GitLab, but where this is not possible, we ask you to set a redirect for that URL to the actual one.
*   We highly encourage your landing page to be in the local language. There are plenty of English-language resources on GitLab, so providing them in the native tongue of your customers adds value.
*   You should use our[ Authorized Reseller Logo](/handbook/resellers/#Logo) on your page, and have it link back to us at about.gitlab.com.
*   Please include a _“What is GitLab?”_ paragraph.
*   Wherever you mention a GitLab product or feature, there should be a link back to our corresponding item on about.gitlab.com.

##### Authorized Reseller Logos

*   The GitLab Authorized reseller logo lets your prospects and customers know that we are working with you.
*   Use our Authorized Reseller Logo on your materials where appropriate and in accordance with our brand guidelines.
*   The logos are available in the README file of the[ resellers project](https://gitlab.com/gitlab-com/resellers/) they may also be accessed in the Marketing collection in the Asset Library in the Partner Portal.


#### Requesting Market Development Funds - MDF

To obtain MDF funding, please complete the following steps prior to any activity being performed or expense incurred. Any activity that does not follow the process below and has the appropriate approvals is not eligible for reimbursement.



*   MDF Request – All MDF requests should be submitted in advance of each planned marketing activity, using the MDF request form found on the GitLab Partner Portal. Please have a conversation with your designated Channel Account Manager for intial agreement prior to submitting the request form in the portal. 
*   Note: MDF requests must be submitted prior to the start of an activity, or the request may be rejected.
*   Please note, as our program is proposal-based, GitLab does not guarantee every eligible partner will receive MDF. In addition, funding levels are subject to change at GitLab’s discretion.
*   MDF Approval – After submission of the MDF request, your GitLab Channel Marketing Manager will review and approve/deny the request. You’ll be notified via email. You must get formal approval before proceeding with your activity.
*   Completing Activity – Partners have the funding quarter and the following quarter to complete approved activities. If at any point within the funding quarter you decide the allocated funds will not be used for the approved activity, you must notify your Channel Marketing Manager.
*   Canceling and Reallocating Funds for an Activity – If the originally approved activity will not take place, please notify your GitLab Channel Marketing Manager. You may be able to reallocate your funds to another activity that is equal to or less than the original approved activity that takes place within the same timeframe.


#####  Proof of Performance (POP) and Claiming Funds

 The GitLab MDF program is proposal-based. For prior approved marketing claims to be reimbursed, the following conditions must be met:


*   You must submit a payment claim within 30 calendar days of completion of the activity via the MDF Reimbursement Request Form on the GitLab Partner Portal.
*   Proof of performance as defined above for each activity must be submitted.  Proof of performance must demonstrate that the activity occurred and that GitLab was appropriately represented
*   International currency: Claims may be paid in local currency upon GitLab’s discretion; currency rates are based on the date GitLab pays the reimbursement.
*   Once a claim is fully approved and all questions about the claim have been answered, you should expect to receive reimbursement within 45 days.

If MDF claims are not submitted in the given timeline, you will forfeit the funds.


##### MDF Terms and Conditions

MDF may not be used for capital expenses or the normal cost of business.

*   Activities/expenses NOT covered under the GitLab MDF Program include: Alcohol, cash, charity event/activities, product discounts, travel and lodging expenses.
*   Activities that involve multiple vendors will not be supported at 100% funding by GitLab. The local Channel Marketing Manager may approve a percentage of funding based on the number of vendors and visibility of GitLab vs. other participating vendors.
*   GitLab reserves the right to modify or terminate the MDF Program upon 30-days advance written notice, which are provided on the GitLab Partner Portal. Such modification or termination will not affect prior approved requests for activities that ran before the effective date of the modification or termination, nor will it affect prior approved plans in progress at the time of the change.
*   If claims are not submitted within 30 calendar days following the activity, the funding allotment and/or allocation may be null and void, and you may forfeit any unallocated or unclaimed funds.
*   GitLab will only pay for the claim amount approved, not fees for wire transfer deposit or local currency deposit.
*   Lead lists provided as proof of performance will not be used by GitLab’s inside sales team to call or nurture unless without written permission from the partner, or the customer has already engaged directly with GitLab.
*   You must conduct your business and marketing operations in an ethical manner and in accordance with all applicable U.S. and international laws, ordinances, codes and regulations. You may not directly or indirectly make, authorize, offer, or promise to make or give any money, bribe, gift or anything of value to any person for the purposes of influencing that person or a family member of that person under circumstances where the payment or gift would constitute an illegal payment under the laws of the United States or any country.
*   All decisions related to program benefits and eligibility are at the sole discretion of GitLab.
*   Partners eligible for MDF must be a GitLab channel partner in good standing, with a valid and current channel partner agreement. Upon termination or expiration of the channel agreement, all in-progress MDF (even if MDF approval has been given) will be forfeited.
*   Submission of false or fraudulent claims can cause you to be ineligible for the MDF Program and may render MDF requests to be forfeited, regardless of their stage of execution.
*   You may not deduct or set off an MDF claim from any payment due to GitLab in the regular course of business with GitLab.
*   GitLab reserves the right to audit and verify all MDF claims and request additional documentation at any time. GitLab may deny any MDF claims that it believes, at its sole and absolute discretion, do not conform to these guidelines.
*   If GitLab subsequently denies an MDF claim for good cause after reimbursement, you must immediately return such funding to GitLab.
*   The terms of these MDF guidelines are in addition to the applicable channel partner agreement in place with GitLab, and are valid only when specifically referenced from the applicable channel partner agreement, program guide, or otherwise made available to a channel party by GitLab in writing. All terms not otherwise defined herein have the meaning set forth in the applicable channel partner agreement.

#####  Entering an MDF Request and Proof of Performance


Partners can access the MDF Request From from the GitLab Partner Portal home page, navigate to Marketing and select “MDF Request.”  Upon selecting MDF Request, complete the first part of the firm to create a fund request. Once completed, submit it and it will be routed to a GitLab Representative for approval within 14 business days. 

Once the marketing or enablement activity is completed, return to the original request and complete the proof of performance documentation as outlined in the Proof of Performance section above. ****


### Technical Support

While not required, we expect customers will, for the most part, contact you if they need help. It is in both of our best interests that they do so, as the more touch points you have with them, the more likely you are to further develop business with them. We do not expect you to be as knowledgeable about our products as our own support staff, and do expect that you may need to escalate some issues to our support staff.

For pre-sales technical issues, please contact your local GitLab sales team


### SECTION 3: Tools and Resources


#### GitLab Partner Portal

GitLab has introduced a new partner portal that serves as the primary source of information and transactions for our partners.  In the initial release, the GitLab Partner Portal will have the following capabilities:



*   Partner registration and account management
*   Deal registration and opportunity management
*   Training accreditation and other learning
*   Sales and marketing resources
*   GitLab news

Visit the portal at partners.gitlab.com.


#### The GitLab Handbook

The GitLab team handbook is the central repository for how we run the company. Printed, it consists of over[ 5,000 pages of text](/handbook/about/#count-handbook-pages). As part of our value of being transparent the handbook is[ open to the world](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook), and we welcome feedback. Although the Portal is your first source of partner information, we often link to the Handbook for detailed information.  If you are unable to find information you need on the Partner Portal, you are encouraged to search pages of GitLab Handbook.


#### Partner FAQ's

**What happens to Partners who don't sign by appropriate deadline (based on original contract date)?** 
* Deal registrations will likely not be approved and thus will not qualify for program discounts. 

**What is the Deal Registration Date?**
* This is the date when a deal registration is submitted via the Partner Portal. 

**For quotes sent prior to the new program announcement, how long will they remain valid?** 
* Quotes will be valid for 30 days, per standared GitLab quote [Guidelines.](/handbook/business-ops/order-processing/) 

**How long are quotes valid under the new program?** 
* Quotes will be valid for 30 days, per the GitLab quote guidelines. 

**If the new contract is signed during the middle of a deal, what pricing discount is used?** 
* If the new program contract is signed before quotes are created, the new program discounts will be used in the quote.  Otherwise, legacy contract discount levels will be used. 

**When did the new program become effective?** 
* The program went into effect April 2, 2020, however Partners will only receive new program discounts and benefits when they execute the contract for the new program. 

**Do Starter/Bronze Licences qualify for the 10% renewal discount?**
* Resale and renewal discounts are not available for Starter/Bronze licenses. 

**When is a license considered new vs. renewal?**
* New licenses = New customer, incremental licenses at existing customers or upgrades for existing customers. 

* Renewal = Contract extension for existing licenses. 

**What is counted towards the $300k target revenue in order to qualify for Select?**
* This includes all the incremental licenses sold to new and existing customers. This includes Starter/Bronze licenses, however the Partner needs to show ability and success in selling/upselling Premium/Silver and Ultimate/Gold to qualify. 


#### Contact Us

All authorized GitLab resellers are invited to the GitLab #resellers Slack channel. channel allows you to reach out to our sales and marketing team in a timely manner, as well as other resellers.

Additionally, you can reach the GitLab channel team at partners@gitlab.com. 
