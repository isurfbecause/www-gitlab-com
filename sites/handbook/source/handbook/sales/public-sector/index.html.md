---
layout: handbook-page-toc
title: "GitLab Public Sector Rules of Engagement"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Government Customers/Contracting
{:.no_toc}

We must ensure all statements and representation to government procurement officials are accurate and truthful, including costs and other financial data. If your assignment directly involves the government or if you are responsible for someone working with the government on behalf of GitLab, be alert to the special rules and regulations applicable to our government customers. Please review the rules below for more details on dealing with the US Federal Government.  Please note that government agencies, in general (US or otherwise) have more strenuous requirements than traditional customers.  Be aware of the heightened sensitivity around dealing with any government entity or foreign official.

Any conduct that could appear improper should be avoided when dealing with government officials and employees or contractors. Payments, gifts, or other favors given to a government official or employee are strictly prohibited as it may appear to be a means of influence or a bribe. Failure to avoid these activities may expose the government agency, the government employee, our company, and you to substantial fines and penalties.

GitLab Public Sector sales team sells EES, EEP, EEU, GitLab Hosted, and custom training directly to:

## U.S. Federal Government
* All Executive, Legislative, or Judicial agencies, departments, commissions, boards, offices, councils, or authorities, including Government Agencies as described above situated in U.S. Territories outside the Continental United States, Hospitals, Medical Centers, and other Health Facilities operated by U.S. Federal Government agencies.
 
* All U.S. Tribal Government entities, including, but not limited to those at below site. 
    * https://www.usa.gov/tribes 
    * This includes For-Profit entities operating under the authority of a government bureaucracy such as Indian Gaming, Casinos under the supervision of the Deputy Asst. Secretary-Indian Affairs for Economic Development and Policy.


## Federal Resellers & Systems Integrators

* All Resellers or Systems Integrators (also referred to as “Prime Contractors”) operating on the behalf of an otherwise-qualified Govt User is considered a Government customer for the specific tasks performed for that End-User.

* Any business unit/wholly-owned subsidiary or division of a public/private company that has as its primary business in providing technology, support and services to the Federal Government.

* All Federally Funded Research and Development Centers (FFRDC’s).  For a current list of FFRDC’s see below link
    * http://www.nsf.gov/statistics/ffrdclist/ 



## U.S. State & Local Government

* State & Local Government: State and local administrative agencies often mirror federal agencies. Thus, the individual states have agencies that control transportation, public health, public assistance, education, natural resources, labor, law enforcement, agriculture, commerce,and revenue. All 50 states, the District of Columbia and each of their agencies (executive, legislative, and judicial) offices, commissions & councils, Indian Tribes, Cities, Counties, Townships, and Special Districts
    * https://en.wikipedia.org/wiki/State_governments_of_the_United_States

* Special districts perform many functions including airports, ports, highways, mass transit, rail systems, parking facilities, fire protection, libraries, parks, cemeteries, hospitals, irrigation, conservation, sewage, wastewater treatment, solid waste, fiber optic systems, stadiums, water supply, electric power and natural gas utility. 
    * https://en.wikipedia.org/wiki/Local_government_in_the_United_States#Special-purpose_local_governments 

* Independent school districts and Higher Education Institutes (both Public and Private Universities)

## FEDERAL GOVERNMENT RULES OF ENGAGEMENT

**I.	INTRODUCTION**
GitLab, Inc. (“GitLab”) will abide by all applicable laws governing federal government contracting. Violation of these laws may result in civil penalties (monetary fines) and/or criminal penalties (fines or incarceration) for GitLab and/or its employees.  In addition, in the event of violation of these laws, GitLab may, in certain circumstances, lose the ability to sell products and services to the Federal Government for a period of up to several years.  
A contractor who is awarded a contract by the Government is deemed to be the prime contractor.  Prime contractors often subcontract some obligations to other subcontractors.  Regardless of whether a company is prime or a subcontractor, they are equally responsible for compliance with applicable rules of the Federal Government of the United States. These rules are entitled the Federal Acquisition Regulations (FAR).  The FAR is intended to regulate procurement during three phases: 1) acquisition planning; 2) contract formation; and 3) contract administration.  
GitLab expects all employees and all agents, vendors, representatives and independent contractors (“Representatives”) involved in federal government contracting to adhere to the highest standards of ethical conduct.  

**II.	OBTAINING FEDERAL GOVERNMENT CONTRACTS**

**A. Procurement Integrity Act:**  Never obtain “off limits” source selection or contractor bid/proposal information from anybody during a competitive procurement.
The Federal Government has rules such as the Procurement Integrity Act (the “PIA”) requiring that competitive procurements be free from any favoritism or unauthorized competitive advantage. Certain types of procurement information are off limits to GitLab during a competitive procurement. It is a crime for a government contractor like GitLab to knowingly obtain sensitive information belonging to its competitors, called “contractor bid or proposal information” or sensitive information generated by the Government called “source selection information” prior to the award of the contract or order to which that information pertains.  

Contractor bid or proposal information includes, but is not limited to, cost or pricing data, direct labor rates and indirect costs, information about proprietary processes, operations or techniques and information marked by the contractor as “contractor bid or proposal information,” in accordance with applicable regulations.  Source selection information includes the prices or costs proposed by competing contractors such as a competitor of GitLab, the agency’s source selection or technical evaluation plans, technical, cost or price evaluations of submitted proposals, competitive range determinations, rankings of competitors or proposals, source selection reports and evaluations, and any other information that the agency marks as “source selection information.”

Therefore, be alert when offered information marked with the following, or similar, legends:
Source selection or procurement integrity sensitive
Company proprietary or trade secrets
For Official Use Only (FOUO)
Not Releasable Under the Freedom of Information Act (FOIA)
Draft - Not For Release Outside of the Government

Always exercise caution and diligence to ensure that GitLab is authorized to receive information provided by Government employees or third parties. You cannot and should not assume that GitLab is permitted to receive all information that it is offered. In doubtful cases, consider requesting that the information be submitted to GitLab with a cover letter on the sender’s letterhead. You should also notify the Legal Department if there is doubt whether GitLab is permitted to possess certain information.  

One example of prohibited information would be receipt and/or possession of confidential and proprietary competitor information offered by a former employee of that competitor.  Information that GitLab cannot and should not possess - should be rejected.
   
**B. Statements and Claims Provided to the Government:** Do not make inaccurate statements or claims to the Government or attempt to cover up false statements or claims.  Everything you say and do must be accurate.  If you are not sure about accuracy, do not make the statement or represent/certify it to be accurate.

GitLab and its individual employees may be liable for civil and/or criminal penalties if an employee knowingly submits a false, fictitious or fraudulent claim such as an invoice to the Government or makes a knowingly false or misleading statement to the Government.  “Knowing” means the person: (1) has actual knowledge of the information; (2) acts in deliberate ignorance of the truth of the information; or (3) acts in reckless disregard of the truth.  Under the law, a “claim” includes any demand or request for money or property, whether or not the United States has title to the money or property requested.  Each invoice submitted by GitLab to the Government is considered to be a “claim.”

A “statement” under the law is basically any statement to a Government employee, including those made orally and in writing.  According to the law, a “statement” includes any representation, certification, affirmation, pricing data/estimates, proposals, time keeping information, sourcing information, document, record, or accounting or bookkeeping entry made with respect to a claim or to obtain the approval or payment of a claim or with respect to a contract with, or a bid or proposal for a contract with the Government.  You can be liable if you: (1) falsify, conceal, or try to hide material fact; (2) make any materially false, fictitious, or fraudulent statement or representation; or (3) make or use any false writing or document knowing it contains any materially false information.

The Government often relies on these certifications to award contracts and contract modifications, and to make payments. In all cases, the Government expects that these submissions, certifications,and representations will be truthful and accurate. Employees must be scrupulous when communicating anything to the Government. An incorrect invoice, record, contract, document or statement can trigger a false claims investigation and pursuit.  Because the penalties for GitLab and its individual employees are so severe, it is extremely important that GitLab employees ensure that statements that they make to Government employees are accurate and are not misleading.  It is also important to make sure that all invoices to the Government are accurate.  	  

All commitments, contracts, representation and certifications made to the Government shall be reviewed by the Legal Department.  GitLab’s Legal will involve other departments such as Human Resources, Finance, Operations or Procurement on an as needed basis. 

**C. Strict Contract Compliance, No Substitution:** Employees must comply with each and every term, condition, specification, and performance requirement contained in a Federal Government contract; Substitution, even for a higher cost or higher quality component by GitLab or our sub/vendor/supplier, on a Government project is strictly prohibited and could be considered a criminal act. 

GitLab must strictly comply with the terms and conditions of its Government contracts, including delivering the exact scope of the contract, specifications, delivery dates, milestones, warranties, insurance and other performance commitments.  Government contracting is not simply traditional private sector commercial contracting with a Federal Government customer.  If there is any deviation required from the express terms of a Purchase Order or contract, get an amendment from the Contracting Officer.  

Once the proposal has been submitted and a delivery order or contract has been awarded to GitLab, strict compliance with the contract is required. For example, the Government may regard non-disclosed substitution of a product or service required by contract specifications as fraud, punishable under the False Claims Act or the False Statements Act. This is an area of high risk, requiring government contractors to be especially alert in ensuring compliance with contract terms. The contractor is responsible to the Government to ensure the integrity of the product and service, as well as the integrity of the related paperwork. The Government also looks to the contractor to ensure vendor and supplier quality. 

The Government is entitled to “compliance” with the technical requirements of the contract. In this regard, it does not matter that the service or product actually furnished is equal to or superior to that described in the contract specifications. Compliance means exactly that, and there are serious risks of noncompliance. Nondisclosure of a material deviation from the specifications of a Government contract, including product or service substitution, may be considered criminal fraud. 

Employees must not deliver inferior quality materials or used materials when new materials are specified; materials that are foreign sourced when domestic materials or their equivalents are certified to by law; or materials that otherwise fail to meet contract specifications. In project fulfillment, the contract must be adhered to strictly and the Contractor is required to ensure there is no substitution of materials or deviation from the contract specifications without express prior approval from the Government’s Contracting Officer. Employees must know the requirements of the contract/subcontract and do exactly what the contract documents require.

Keep in mind that there are some government provisions that always apply even if not expressly called out in the Purchase Order or contract.  These “flow-down” provisions apply to all contractors, even those that are not prime.  

Subcontractors on a government contract must comply with all required terms and conditions, as well.  If you seek to have a subcontractor perform or provide under a government contract, ensure the applicable terms and conditions are in play.  In addition to the mandatory flow-down requirements, the government has recently initiated new requirements surrounding Human Trafficking/Slavery, Counterfeit Goods and Conflict Minerals.  Any contracts with subcontractors should set out the fundamental expectations around these areas. 

**D. Organizational Conflicts of Interest:**  Employees should avoid scenarios that create the appearance or reality of an organizational conflict of interest.
GitLab may be prohibited or restricted from performing work for the Government if the Government determines that there is an organizational conflict of interest (“OCI”).  An OCI is created when a contractor has an unfair competitive advantage in winning a job or is unable to perform the work in an objective manner.  For example, if a contractor is asked, under a contract, to evaluate work that it or one of its affiliates completed in a prior contract, an OCI would be said to exist.  Or, if a contractor helped the Government write a request for proposal, that contractor would not be allowed to bid for the contract because it would have an unfair advantage over all other competitors.  There are other situations that create OCI’s and each situation must be considered based upon the facts. 

Occasionally, the presence of an OCI clause in a request for proposal or request for bid may warrant a contractor to “no-bid” a federal government job, if the Government will not limit or clarify the scope of the OCI clause.  For example, if work on a smaller job today will prevent a company from bidding on a much larger job down the road, it may be wise for the company to “no-bid” the smaller job.  However, often, contractors can work with the Government to limit the likelihood that performing one contract will prevent the contractor from being able to propose for other contracts.  Federal agency heads have the express ability to waive an OCI restriction if they determine that its application in a particular situation is not in the Government’s interest.  Nevertheless, a significant risk in this area is inadvertently agreeing to a broad OCI clause in a federal government contract without analyzing or attempting to negotiate its preclusive impact on future federal government contracting opportunities.  Therefore, because OCI restrictions can impair a contractor’s future business prospects, early identification of potential OCI issues is critical.

**E. Personal Conflicts of Interest:** Employees should avoid scenarios that create the appearance or reality of a personal conflict of interest.
Certain federal laws prohibit government employees from placing themselves in situations that may create actual or perceived personal conflicts of interest.  Although these laws govern the behavior of government employees, and not contractor employees, it is important to understand how personal conflicts of interest are created so as to avoid inadvertently creating such a situation.    
Situations that may cause personal conflicts of interest are quite varied, however, a few common situations tend to occur frequently.  For example, if a government employee participates “personally and substantively” in any matter in which she or her family has a financial interest, a personal conflict of interest may exist. In this regard, for example, if a government purchasing agent awards GitLab a contract and his wife or brother works at GitLab, a possible personal conflict of interest exists.  If, a government purchasing agent awards a contract to GitLab at the same time that he is speaking with GitLab about a possible job, a personal conflict of interest may be created.  

**F.	Anti-Kickback Act:**  Do not give or receive anything of value to obtain favorable treatment in connection with Government contracting.
It is a crime to: (a) provide, attempt to provide or offer to provide, or (b) solicit, accept or attempt to accept, any kickback with respect to any federal government contract or subcontract.  It is also illegal to include, directly or indirectly, the amount of any kickback paid into the contract price to be charged by a subcontractor to a prime contractor or a higher-tier subcontractor under a federal government prime contract, or in the contract price charged by a prime contractor to the Government.  So, with regard to kickbacks, it is illegal to give them or receive them and it is illegal to seek to obtain reimbursement for them from the Government or a prime contractor.

A kickback is any money, fee, commission, credit, gift, gratuity, thing of value, or compensation of any kind which is provided, directly or indirectly, to any prime contractor, prime contractor employee, subcontractor, or subcontractor employee for the purpose of improperly obtaining or rewarding favorable treatment in connection with a prime contractor in connection with a subcontract relating to a prime contract.   Accordingly, many arrangements, such as a fee-commission-splitting arrangement or the giving of gifts or payment of money, intended to “cement a relationship” or in any way confer a benefit of any kind on any person or entity involved in a contract, a bid or the performance of a contract relating to a federal government procurement, may potentially be determined to be a kickback. 

**G. 	Contingent Fees:**  Do not pay contingent fees to third parties to facilitate the obtaining of Government contracts.

Federal law prohibits a government contractor from paying fees for help in obtaining federal government contracts if the payment of the fees is conditioned or contingent upon the award of the contract to the government contractor.  

Most negotiated contracts must contain a warranty by the contractor, called the “Covenant Against Contingent Fees,” affirming that no person or agency has been employed or retained to solicit or obtain the contract on a contingent fee arrangement.

The question of whether a contingent fee is being paid or has been proposed to be paid arises frequently when a federal government contractor contemplates hiring or working with a consultant that promises to assist the government contractor in business development activities.  It is important to recognize that, absent the applicability of certain statutory and regulatory exemptions, the payment of fees that are contingent upon the successful award of federal government contracts are considered to be illegal contingent fees.       

**H. 	Independent Pricing:**  Antitrust laws apply to transactions with the Government, just as they do to transactions in the commercial sector. Because the Government acts in dual roles when it contracts -- it is both the buyer and the law enforcer -- the antitrust enforcement risk may be even greater in federal government contracting than in commercial contracting.
Anticompetitive practices of price-fixing (agreements between competitors to fix prices), market allocation (agreements between competitors to divide up customers or territories), bid rigging, follow-the-leader pricing (where all the competitors adopt the market leader’s prices), rotated low bids (agreements where each competitor takes a turn as the low bidder) and collusive price estimating systems are illegal, and can lead to criminal as well as civil actions against individuals involved as well as against the company.  

GitLab employees should avoid discussions with GitLab’s competitors regarding GitLab business, and should never discuss pricing or prospective business with competitors.  The pricing in proposals submitted to the Federal Government must be arrived at independently without consultation, communication or agreement with any other offeror, competitor or profits/losses associated with any other contract.

The pricing in proposals submitted to the Federal Government must be arrived at independently without consultation, communication or agreement with any other offeror, competitor or profits/losses associated with any other contract. Bids may not be in collusion with or consultation with competing offerors and all pricing must be arrived at independently. If GitLab is bidding on a Government project to more than one competing prime contractors, a GitLab bid may be submitted to each prime contractor, but each bid must be at the same price. Employees participating in a solicitation must comply with the provisions of the Federal Acquisition Regulations governing Independent Pricing, prior to the submission of a bid in response to a Federal Government solicitation. Independent pricing compliance requires the following:
		
*a)* The prices have been arrived at independently, without, for the purpose of restricting competition, any consultation, communication, or agreement with any other offeror or competitor relating to communication, or agreement with any other offeror or competitor relating to (i) those prices, (ii) the intention to submit an offer, or (iii) the methods or factors used to calculate the prices offered;
*b)* The prices in the offer have not been and will not be knowingly disclosed by the offeror, directly or indirectly, to any other offeror or competitor before bid opening (in the case of a sealed bid solicitation) or contract award (in the case of a negotiated solicitation) unless otherwise required by law; 
*c)* Pricing was derived separately and independently and does not use proposed price reductions under other contracts as an evaluation factor.  Also, do not consider losses or profits realized or anticipated under other contracts;
*d)* Pricing does not include any amount for a specified contingency to the extent that the contract provides for a price adjustment if that contingency occurs; and
*e)* No attempt has been made or will be made by the offeror to induce any other concern to submit or not to submit an offer for the purpose of restricting competition.  

**l. 	Prohibition on Subcontractors’ Restriction of Sales to the Government:**  Do not prevent subcontractors from permanently selling directly to the Government on an on-going basis.
Federal law prohibits prime contractors from unreasonably restricting direct sales by their subcontractors to the Government under a prime contract or follow-on production contract. This prohibition does not, however, prevent a prime contractor from prohibiting its subcontractor from: (1) directly communicating with the prime contractor’s federal government client concerning the particular prime contract; or (2) interfering with the existing contractual relationship between the prime contractor and a Government.  


**III.	DEALINGS WITH FEDERAL GOVERNMENT PERSONNEL**

**A. Bribery and Gratuities:**  No GitLab employee shall directly or indirectly offer, pay, loan or give anything of value to any Government official or employee, for the purpose of obtaining business or influencing the official or employee.
It is a crime to give, offer or promise a bribe or gratuity to a federal government official.  A bribe can be anything of value given, directly or indirectly, offered or promised, corruptly and with intent to influence an official act or omission, cause the perpetration of a fraud upon the Government,.  A gratuity is anything of value given, offered or promised for or because of any official act performed or to be performed.  No government employee may solicit or accept, directly or indirectly, any gratuity, gift, favor, entertainment, loan, or anything of monetary value from anyone who (a) has or is seeking to obtain federal government business with the employee’s agency, (b) conducts activities that are regulated by the employee’s agency, or (c) has interests affected by the employee’s official duties. 
   
**B.	Gifts to Government Employees and Officials:**  Avoid giving gifts to Government employees or officials.
Government employees are prohibited by government ethics rules from accepting, from a “prohibited source,” cash and cash equivalents; gifts anything of value – even when it is not offered or accepted in exchange for official acts or.  A “prohibited source” is any person who: (1) does business or seeks to do business with the employee’s agency; (2) conducts activities regulated by the employee’s agency; (3) is seeking official action by the employee’s agency; or (4) has interests that may be substantially affected by the performance or nonperformance of the employee’s official duties.  In particular, a government contractor, such as GitLab, and its employees and Representatives is a “prohibited source” with regard to those federal government agencies, departments and entities with which GitLab is conducting or seeking business. 
 
**C.	Hiring Former Government Employees:**  Do not solicit Government employees or officials with whom GitLab has or is pursuing a relationship.
A number of government conflict of interest laws restrict what a former government employee may do after leaving federal government service and joining the private sector. These so-called “revolving door” laws also regulate the timing and manner of employment discussions.  Employment discussions with a procurement official during the course of a federal agency procurement pose special risks that may lead to a contractor’s disqualification from the procurement, among other sanctions.  As a general rule, a contractor cannot have employment discussions with a current government employee if that employee is personally participating in or supervising matters concerning that contractor.    

Upon joining the private sector, a former government employee may also be subject to various post-employment restrictions, such as: the former government employee is 1) prohibited for life from representing a contractor before his or her former agency or any other government agency relating to any matter in which he or she personally participated during government service; 2) the former government employee is prohibited for two years from representing a contractor before his or her former agency or any other federal government agency relating to a matter that was pending under his or her official responsibility during his or her last year of government employment; 3)a former senior-level government employee is prohibited for one year from communicating with, or appearing before, any employee of his or her former agency with the intent to influence that employee on behalf of a federal government contractor; or 4) there may be a one-year “compensation ban” on certain government procurement officials.      

**D. 	Lobbying:**  Contractors must carefully consider to whom they market their abilities when such marketing calls are not in response to a solicitation.
Contact with certain government employees with the intention of influencing procurement decisions or obtaining government contracts may trigger reporting and certification requirements under the Lobbying Disclosure Act.  

To trigger the reporting requirements, the contractor personnel making the contact must qualify under the Lobbying Disclosure Act as a “lobbyist.” An individual qualifies as a “lobbyist” if that individual makes a contact intended to influence a high ranking member of the Government and if lobbying activities (preparing for and participating in lobbying contacts) constitute at least 20% of the individual’s time within a three month period.  However, response to a formal solicitation or published request for information is an express exception to the definition of a “lobbying contact.”

Costs associated with lobbying activities are unallowable costs in cost reimbursement contracts.

**E. 	Handling Government Classified Information:**  Appropriate clearance, a need to know and compliance with caveats are required prior to releasing/reviewing sensitive information

The Government has varying degrees of secrecy surrounding the sensitivity of its information.  This sensitivity is based on a calculation of the risk of damage to national security if the information were to be released.  

The three levels of classifying information, in increasing order of sensitivity are: confidential, secret, and top secret. 

If no such security level has been assigned to a given set of information, it is deemed “unclassified”. “Declassified” means a previously established security level has been removed. “Downgraded” means the information has been reassigned a lower security level.  The Government has mechanisms in place which automatically downgrade certain documents after a set number of years.  Do not assume declassification without first confirming with GitLab’s Legal Department. The Government has several other special caveats for sensitive information whose dissemination is controlled but not classified.  

When handling Government information, there are factors that must be considered in conjunction with any given classification including the individual’s and facility’s security clearance and the need-to-know the information. **Appropriate clearance, a need to know and compliance with caveats are required prior to releasing, reviewing or accessing sensitive information.**

***Security Clearance*** - A security clearance authorization mirrors the grades of classified information and enables a person to handle information up to and including their security clearance.  (e.g., A Top Secret security clearance provides access to Top Secret, Secret and Confidential; and a Secret clearance provides access to Secret and Confidential.) However, having a Top Secret clearance does not give one access to all documents classified at that level. Rather, information is released according to security clearance and the “need to know”.  

***Need to Know*** - Information that is classified can only be provided on a "need to know" basis. “Need to know” means that a person is only entitled to see information if a determination by the authorized holder of the classified information ensures that a prospective recipient requires access in order to perform or assist in a lawful and authorized governmental function. 

***Marking*** - If the government provides a user with sensitive information, it is generally easy to recognize.  Classified Government documents are typically required to be stamped with their classification on the cover and at the top and bottom of each page. Often each paragraph, title and caption in a document may be marked with the highest level of information it contains, usually by placing appropriate initials in parentheses at the beginning or end of the paragraph. 

Oftentimes, a brightly-colored cover sheet may be affixed to the classified document. This should remind the user to properly protect the document and to minimize risk of observation of someone unauthorized in the vicinity. Never remove, destroy, conceal or otherwise alter any Government marking.

***Caveats*** - Restrictive caveats can be added to classified documents by the government.  These caveats further restrict the release of information to those who have the appropriate clearance level and possibly the need to know the information. If you violate these directives, you may be guilty of violating a lawful order or of mishandling classified information.

For ease of use, code words or abbreviations have been adopted that can be included in the summary classification marking (header/footer) to enable the restrictions to be identified at a glance. Some of these codes are:

* NOFORN — Distribution to non-US citizens is prohibited, regardless of their clearance or access permissions. 
* RESTRICTED — Distribution to non-US citizens or those holding an interim clearance is prohibited; certain other special handling procedures apply. 
* NOCONTRACTOR — Distribution to contractor personnel (non-US-government employees) is prohibited, regardless of their clearance or access permissions. 
REL TO <country code(s)> — Distribution to citizens of the countries listed is permitted, providing they have appropriate access and need to know. Example: "REL TO AUS, GBR, CAN, NZ" indicates that the information may be shared with appropriate personnel from Australia, Great Britain, Canada, and New Zealand. 
* <nn> X <m> — Information is exempt from automatic declassification (after the statutory default of 10 years) for exemption reason <m>, and declassification review shall not be permitted for <nn> years (up to 25). Example: "25X1" indicates the information must remain classified for 25 years, since it pertains to intelligence activities, sources, or methods (reason "1"). 
* ORCON — Originator controls dissemination and/or release of the document. Classification level and caveats are typically separated by "//" in the summary classification marking. For example, the final summary marking of a document might be: SECRET//<compartment name>//NOFORN//ORCON//25X1

***Storing*** - All classified government information must be securely stored.  This means that the documentation must be locked up and stored in a manner not accessible by unauthorized parties. 

***Sending*** - There are restrictions on how classified documents can be shipped. Top Secret material must go by special courier. Secret material can be sent within the U.S. via registered mail, and Confidential material by certified mail. Electronic transmission of U.S. classified information requires the use of government approved encryption systems. Please contact GitLab’s Legal if there is any intent to handle Secret or Top Secret information.

***Destruction*** - Classified Information must be carefully handled throughout its life. DO NOT THROW CLASSIFIED INFORMATION AWAY.  Classified Information must be retained in accordance with the GitLab records retention practices or as contracted with the government, whichever is longer.  The destruction of certain types of classified documents requires burning, shredding, pulping or pulverizing using approved procedures and must be witnessed and logged.

**F. Protecting Proprietary Data:**  Proprietary data, trade secrets, and confidential business or financial information authorized to be disclosed to the Government must be marked with the proper language.

The Government will often seek to obtain a contractor’s proprietary, technical information and confidential business or financial information, such as source codes, designs, and processes relating to the delivered equipment, technology or process in contracts and confidential or sensitive financial information. More importantly, the Government also often seeks rights in the data. The Government may seek to obtain:
*Unlimited rights* – this means the Government has flexibility to share information internally and with third parties, including providing the data to competitors.
*Limited rights* - this constrains the Government to use the item only for certain specific government purposes or can be restricted as applied to software.
*Government purpose rights* - rights are limited rights that turn into unlimited rights after a specified period of time.

**Any solicitation requesting rights to proprietary or technical data, trade secrets, and confidential business or financial information must be promptly forwarded to the Legal Department for review.** Appropriate legends must be placed on proposals and other sensitive information so that others, including the Government, will be on notice that GitLab considers such information to be business sensitive, proprietary, confidential and not releasable outside of the Government. Such legends also put our competitors on notice that they are not authorized to receive -information GitLab considers to be proprietary and confidential. The following legend should be placed on proprietary and confidential information submitted to the Federal Government, either directly or through another party:  “GitLab Trade Secrets/Proprietary/Confidential. Not Releasable Outside the Government or Under FOIA.”
	
**IV.	SOCIOECONOMIC CONSIDERATIONS**

The Government uses federal government contracts as a vehicle for the advancement of important socioeconomic policies.  The primary policies and government regulations implementing these policies are set forth below.

**A.	Non-Discrimination and Equal Employment Opportunity:**  Do not discriminate against any employee or applicant for employment because of race, color, religion, sex or national origin.
Government contracts contain various nondiscrimination and equal employment opportunity clauses.  The Equal Opportunity Clause mandates, among other things, that government contractors will not discriminate against and will ensure fair treatment of any employee or applicant for employment because of race, color, religion, sex or national origin (“Protected Status”); Contractors will provide notice by posting, in conspicuous places available to employees and applicants for employment, explanation of the equal opportunity clause and state compliance in all solicitations or advertisements for employees placed by or on behalf of the contractor. GitLab is an equal opportunity employer and complies with federal and state employment laws and regulations.  Equal Opportunity is expressly addressed in the Code of Conduct and Ethics.  

**B.	Drug-Free Workplace:** Support and maintain a drug free workplace.  
Another workplace protection law that is unique to government contractors is the Drug-Free Workplace Act of 1988.  This Act requires contractors to (1) publish a policy statement prohibiting the unlawful use or possession of drugs in the workplace, (2) establish a drug-free awareness program, (3) promptly notify the Government if any employee working under a contract subject to the Drug-Free Workplace Act is convicted under a criminal drug statute for a violation occurring in the workplace,  (4) impose sanctions on employees who are convicted of drug abuse violations occurring in the workplace, and (5) make a good faith effort to maintain a drug-free workplace through implementing the above requirements.

**V.	PAYMENTS TO FOREIGN OFFICIALS** 

**Anti-corruption:** Do not attempt to bribe any foreign official.  Do not falsify any accounting records including expense reports, vouchers, ledgers or any other financial documents.
Anti-Corruption legislation, such as the Foreign Corrupt Practices Act (“FCPA”), OECD Anti-bribery Convention initiatives, the UK’s Bribery Act, and other similar anti-corruption legislation, effectively prohibits all payments (including offers or promises to pay) and gifts to foreign officials for the purpose of obtaining or retaining business, influencing an official act or decision, or inducing an official to do or not to do any act in violation of the individual’s official duties.  The term “foreign official” means any officer or employee of a foreign government or any department, agency, or instrumentality thereof, or of a public international organization, or any person acting in an official capacity for or on behalf of any such government or department, agency, or instrumentality, or for or on behalf of any such public international organization.  In addition, the FCPA contains certain recordkeeping and internal accounting requirements. 
VI.	REPORTING DEFAULT OR TERMINATION

**V.I. Report Default or Termination:** Immediately report performance default or potential termination of a Federal Government contract or delivery order to the Legal Department.

Termination for default of a Federal contract would be a significant negative event for GitLab, placing a black mark on performance and burdening any future contract awards. It can also subject GitLab to financial costs, contract loss and possible criminal sanctions depending on the nature of the default. In addition, our sales efforts in the state and local governmental arena and private sector markets can be adversely affected by the termination for default of a federal contract, because many prospective customers require GitLab to disclose such terminations during the competitive procurement process.

Upon receiving any indication of performance issues or any other evidence that a GitLab Federal Government contract may be in danger of being terminated for default, immediate action as outlined below is required. The default or termination process has three variations increasing in significance.

*Notice 1* – The Government’s Contracting Officer may send an email or letter to GitLab stating that there are performance issues with a particular delivery order and request the contractor to become engaged in the problem. The Contracting Officer may also mention orally that there are some problems on the project. The notification from the Contracting Officer may not mention the terms “Cure Notice” or “Show Cause” or “Delinquency,” but if the intent is to convey performance problems on a Federal project it requires an immediate attention.

*Notice 2* - Contracting Officer may issue a Cure Notice to GitLab specifying the failure to perform and giving the contractor, for example, 10 days to cure the failure. If the contractor failed to deliver an item or perform on time, the government need not give any opportunity to cure, it may simply proceed to Notice 3.

*Notice 3* – Contracting Officer issues a Show Cause Notice, which places the burden on GitLab to demonstrate why the Delivery Order should not be terminated for default.

**Any of the above notices require immediate and formal responses.**  Upon receipt of any of the notices addressed to GitLab directly from the Government outlined above, GitLab is required to respond to the Contracting Officer or other Government official immediately in writing. The letter must be approved by and issued from the Legal Department.

In the event you receive any **written or oral** notice of delinquency, failure to perform, a cure notice or a show cause notice from a Federal Government customer – immediately contact GitLab’s Legal Department.

**VII.	   COMPLIANCE** 

**A. Mandatory Disclosure Obligations:** As there are violations that require mandatory reporting, contact the Legal Department or the Hotline immediately with known or suspected violations.

For several decades, the concept of voluntary disclosures in government contracting seemed to work well for the procurement community.  Neither the Government nor contractors saw a need to change the system and the prevailing attitude was more or less captured by the old adage, “if it ain’t broke, don’t fix it.”  However, beginning in 2007, the Criminal Division of the United States Department of Justice believed the system needed to be “fixed.”  As a result, such disclosures are now mandatory, not voluntary. 
Contractors are subject to mandatory disclosure obligations for specific contract and subcontract-related violations of certain laws.  In the event you become aware of a known or suspected violation that would warrant disclosure, contact Legal Department or the Hotline immediately.      	 

**B. 	Records Retention and Full Cooperation with Government Audits and Investigations:** Maintain records per the contract term.  Cooperate with government auditors and investigators. 
The Government generally reserves the right to examine or audit contractors’ records including books, documents, accounting procedures, and practices and other written or electronic data. Accordingly, it is GitLab’s policy to retain all records related to contract performance for four years following the final contract payment, or other retention periods if provided in a particular contract or applicable law.  
Government contractors are required to provide “[f]ull cooperation with any government agencies responsible for audits, investigations, or corrective actions.”  By regulation, “full cooperation” is defined as disclosure to the Government of the information sufficient for law enforcement to identify the nature and extent of the offense and the individuals responsible for the conduct, including providing timely and complete response to government auditors’ and investigators’ request for documents and access to employees with information.  These cooperation obligations do not prevent GitLab from conducting its own investigation, maintaining its attorney-client privilege or defending itself in a legal proceeding.   

**C.	Penalties for Noncompliance** 

*1. For the individual Employee, the Government may:*
a) impose civil and criminal penalties on the individuals involved; 
b) civil penalties may be enforced against a person of up to $250,000, depending on the violation,  for each violation by the individual, plus twice the amount of compensation received or offered, for any violation of the above prohibitions; and/or
c) criminal penalties of up to ten (10) years imprisonment, depending on the offense; and fines.

*2. For GitLab, the Government may:*
impose civil and criminal penalties; 
civil penalties may be enforced against an organization upwards of $500,000, plus twice the amount of compensation received or offered, for each violation of the prohibitions. 
disqualify GitLab from a competitive bid; 
cancel a procurement, if a contract has not been awarded; 
 rescind (declare void) any tainted contract that has been awarded;  
suspend or debar GitLab, which prohibits GitLab from submitting an offer or taking a new order from the Federal Government; and
assign auditors or mandate enhanced compliance requirements.  

**D.	Reporting or Guidance**
GitLab employees have a number of ways to resolve legal and ethical questions and report actual or suspected legal or ethical violations that come to their attention regarding federal government contracting.  If you know or suspect of any violation of any requirement in this Guide, report the matter to your manager, the Legal Department, or through the Hotline. 

If there are any questions about any of the above, please contact GitLab’s Compliance Officer at (Compliance@gitlab.com).

## DPAS Rated Orders

The Defense Priorities and Allocations System (DPAS) prioritizes national defense- related contracts/orders throughout the U.S. supply chain in order to support military, energy, homeland security, emergency preparedness, and critical infrastructure requirements.

Any entity who places or receives government rated contracts should be well versed and in compliance with the DPAS regulations. 

There are two levels of priority:
The highest level is “DX”, which represents the highest national industrial priority.
The next level is “DO” which is critical to national defense.  

DX rated orders are equal to one another but take priority over all DO and non-rated orders.
DO rated orders are equal to other DO orders but take priority over non-rated orders.

Each DPAS priority rated contract or purchase order must contain the following:
• Priority Rating (Rating symbol (DX or DO) + Program ID)
• Required Delivery Date or Dates
• Authorized Written or Digital Signature
• Certification Statement—A statement that reads in substance:
This is a rated order certified for national defense use, and you are required to follow all the provisions of the Defense Priorities and Allocations System regulation (15 CFR Part 700).

Priority ratings are a four digit identifying code assigned by a delegate agency or authorized person placed on all rated orders and consisting of the rating symbol – DO or DX - and the program identification symbol, such as A1, A7, or C9.  Program identification symbols indicate which approved program is supported by a rated order they do not represent any particular priority.

Invalid identification symbols are not eligible for priorities and allocations support under the Commerce-administered DPAS. The list of approved programs and their identification symbols are listed in Schedule 1 of the DPAS regulation.

**Mandatory Acceptance of Rated Orders**: All US companies must accept and fill all rated orders for items that the company normally supplies. A company may not discriminate against rated orders in any manner, such as by charging higher prices or by imposing different terms and conditions than for similar unrated orders. 

**Mandatory Rejection of Rated Orders**: A company may not accept a rated order for delivery on a specific date if unable to meet that date. However, the company must inform the customer of the earliest date on which delivery can be made and offer to accept the order on the basis of that date. The existence of previously accepted unrated or lower rated orders is not sufficient reason for rejecting a rated order.

**Optional Rejection of Rated Orders**: A company may reject a rated order in any of the following cases as long as the supplier does not discriminate among customers: 
 if the person placing the order is unwilling or unable to meet regularly established terms of sale or payment;
if the order is for an item not supplied or for a service not performed; 
if the order is for an item produced, acquired, or provided only for the supplier’s own use for which no orders have been filled for two years prior to the date of receipt of the rated order. If, however, a supplier has sold some of these items, the supplier must accept rated orders up to the quantity or portion of production, whichever is greater, sold within the past two years; or 
if the person placing the rated order, other than the U.S. Government, makes the item or performs the service being ordered.

**A company must accept or reject a rated order and transmit the acceptance or rejection (in writing or electronically) to the customer placing the order within 15 working days after receipt of a “DO” rated order and 10 working days after receipt of a “DX” rated order. If the order is rejected, a company must provide the reasons for the rejection.**

A company must schedule its production or operations, including the acquisition of all needed production items, to satisfy the delivery requirements of each rated order you receive.  
If a company has accepted a rated order and subsequently finds that the shipment or performance will be delayed, the company must notify the customer immediately, give the reasons for the delay, and advise of new shipment or performance date.

A company must reschedule unrated orders if they conflict with performance against a rated order and must reschedule “DO” rated orders if they conflict with performance against a DX rated order.

A company who receives rated orders must in turn place rated orders with its suppliers for the items needed to fill the rated orders. The priority rating must be included on each successive order placed to obtain items needed to fill a customer’s rated order. This continues from contractor to supplier throughout the entire supply chain.  A company is not required to place a priority rating on an order for less than $75,000 provided that delivery can be obtained in a timely fashion without the use of the priority rating.  Please contact Compliance if the need to subcontract arises. 

Company staff should check Schedule 1 to Part 700 of the DPAS regulation to ensure the company has received a proper rated order. If the company receives a rated order with a program identification symbol not listed in Schedule 1, consequently the order is not properly rated and the company should promptly notify the company who placed this priority rating or the appropriate contract administration officer for guidance or assistance.

A priority rating on a rated order may be changed or canceled by written notification from the company who placed the rated order.  If a company no longer needs items to fill a rated order, any rated orders placed with suppliers for the items, or the priority rating on those orders, must be cancelled.

A company is required to make and preserve for at least three years, accurate and complete records of any transaction covered by the DPAS regulation.

## Protection of customer information

At GitLab the [Transparency value](https://about.gitlab.com/handbook/values/#transparency) extends to what we are doing and how we do it, but that value is not pushed down onto customers and external contacts.  The GitLab Public Sector team, in particular, extends that exception to all customer data even within the larger GitLab organization.  Public Sector customer data is isolated and can only be accessed by those GitLab team members with a need to know.

