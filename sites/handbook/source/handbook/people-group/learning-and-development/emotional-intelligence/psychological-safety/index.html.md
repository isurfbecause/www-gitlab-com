---
layout: handbook-page-toc
title: Psychological Safety
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

On this page we are going to review psychological safety and it's importance for successful teams. 

## What is Psychological Safety

Psychological safety is [defined by Amy Edmondson](https://www.jstor.org/stable/2666999?seq=1) as a “shared belief held by members of a team that the team is safe for interpersonal risk taking”.

It's not about being warm and fuzzy and sharing your feelings. It's about being comfortable admitting when you are wrong or have made a mistake as well as challenging each other for the better. 

Watch the video below where Susan David explains psychological safety. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/O0kAMpRp2hU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

## How it Works

The following information was summarized from an [article on Psychological Safety](https://peopletalking.com.au/project/psychological-safety/) by PeopleTalking. 

**Psycholgoical Safety** has primary importance when it comes to significant impact on a team's performance. 

The other factors that have an impact on a team's performance are: Dependability, Structure, Meaning & Clarity, Impact. 

In addition, the factors of Accountability, Open Communication & Motivation, Dialogue Practices, and Shared Assumptions are only valuable when the team is already psychologically safe. In the diagram, those lie on the arrows between each of the circles.   

Why is psychological safety important? When you have psychological safety in the workplace, the following things increase: 

- Collaboration
- Innovation
- Inclusion
- Wellbeing
- Culture

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQn6_E_jGgOUz9OBe0hYopG5MYF4k6-MV2NsdESCpxjYenS9ikKD8mylL_Id44GXzl5-lHLltwHsWLD/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

Amy Edmonson uses David Kantor's model to show the [relationship between Psychological Safety and Accountability & Motivation](https://peopletalking.com.au/project/psychological-safety/). When there is an environment where psychological safetey is low or non-existant, it can be very stress or anxiety filled. When there is high psychological safety, it can make more comfortable. You can get a better picture of this from the diagram below. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRvy9_S6SM97risN1JCRC4Hy0rTRt3PQXdMhl_D_xxEFi-W4F5FYL04FrZp62HgrPlt6N-O8Um-gnCk/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Psychological Safety vs Danger

When you have psychological safety people become more motivated because they feel more able to take risks. When people feel psychologically safe, they will learn from their failure rather than feeling the need to blame others. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRzAtUx8ptPA5FbZ1Pn_GKRQB_9I_K98Sxr7GBfssU3FxpkVUXCA9kyg7j5xWV6X9NEOR0zC7SdGuJb/embed?start=false&loop=false&delayms=60000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Diversity and Inclusion

Psychological safety has impacts on diversity and inclusion in organizations. Even if an organization has diversity, it doesn't mean that inclusion is present as well. 

## Strategies for Cultivating a Culture of Psychological Safety

1. Embrace a culture of respectful debate.
1. Encourage personal storytelling. 
1. Ask questions.
1. Allow for experimentation and failure.
1. Dismantle perceptions of hierarchy.
1. Model openness to feedback
1. Set clear goals and key performance indicators.
1. Offer development opportunities.
1. Build a speak-up culture.
1. Highlight competencies.

The above list is from a [Grant Thornton article](https://www.grantthornton.com/library/articles/advisory/2020/psychological-safety-speak-up-culture.aspx).

It is important to enable a human-to-human approach and realize the other party is more like you than different. The reflection activity is called “Just Like Me” [developed by Paul Santagata](https://hbr.org/2017/08/high-performing-teams-need-psychological-safety-heres-how-to-create-it) asks you to consider: 

- This person has beliefs, perspectives, and opinions, just like me.
- This person has hopes, anxieties, and vulnerabilities, just like me.
- This person has friends, family, and perhaps children who love them, just like me.
- This person wants to feel respected, appreciated, and competent, just like me.
- This person wishes for peace, joy, and happiness, just like me.

## Live Learning Session 

During Week 3 of our [Manager Challenge Pilot](/handbook/people-group/learning-and-development/manager-challenge/), we had a course covering Building an Inclusive & Belonging Environment. The [slide deck](https://docs.google.com/presentation/d/1eBr33VeYePGck9K5Q5WGqrhqKmKT8zLgkO3kNNRCew0/edit?usp=sharing) and [meeting agenda](https://docs.google.com/document/d/13BofXLCDC2fafHlYij7sZetBidB2cycuz1BeJSSDsMo/edit?usp=sharing) follow along with the session. 

A recording of the second of two sessions can be found here:

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/pdB-WWeXNjg" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## Additional Resources 
- Read: [The Role of Psychological Safety in Diversity and Inclusion](https://www.psychologytoday.com/us/blog/the-fearless-organization/202006/the-role-psychological-safety-in-diversity-and-inclusion) - Psychology Today 
- Read: [The Fearless Organization by Amy C. Edmondson](https://www.amazon.com/Fearless-Organization-Psychological-Workplace-Innovation/dp/1119477247/ref=sr_1_1?keywords=The+Fearless+Organization&qid=1567701716&s=gateway&sr=8-1)
- Read: [7 ways to create psychological safety in your workplace](https://blog.jostle.me/blog/7-ways-to-create-psychological-safety-in-your-workplace)
- Read: [What Google Learned from it's Quest to Build the Perfect Team](https://www.nytimes.com/2016/02/28/magazine/what-google-learned-from-its-quest-to-build-the-perfect-team.html)
- Read: [How To Build An Environment Of Psychological Safety](https://trainingindustry.com/articles/compliance/how-to-build-an-environment-of-psychological-safety/)
- Read: [How Psychological Safety Actually Works](https://www.forbes.com/sites/shanesnow/2020/05/04/how-psychological-safety-actually-works/#c5be718f864c)
- Listen: [Creating Psychological Safety in the Workplace](https://hbr.org/podcast/2019/01/creating-psychological-safety-in-the-workplace) - HBR 
- Watch: [Why good leaders make you feel safe - Simon Sinek](https://www.youtube.com/watch?v=lmyZMtPVodo&list=PLhVoCCkYxQDcdcU7j7H_w0-0BdOqIIYxa) - TED

The SSOT for the slides on this page can be found in [this folder](https://drive.google.com/drive/folders/1RLXcBUM1IKx8KmgQSR-dPDwvkso6bVJL).
