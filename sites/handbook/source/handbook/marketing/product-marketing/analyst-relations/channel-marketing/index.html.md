---
layout: handbook-page-toc
title: "Channel Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## When to engage a channel partner with Channel Marketing

As this expanded program has just launched end of April 2020, a lot of these processes for how to engage are not yet documented and will be more ad hoc in nature as more Channel Account Managers (CAMs) come on board.  Channel Marketing and [Field Marketing Managers (FMMs)](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing--channel-marketing) will be working together to identify processes and iterate on those processes for the most efficient way to work with each other, the CAMs and the Channel Partner

## 🚧 ️What are we working on
Check out our [Channel Marketing board](https://gitlab.com/groups/gitlab-com/-/boards/1779611?label_name[]=Channel&label_name[]=Channel%20Marketing) with all the details of what we have inflight

## 📝 How to request Channel Marketing Support
*Examples of what Channel Marketing executes:*
* Channel Partner hosted webcasts
* GitLab hosted webcasts with channel partners
* Channel Partner guest blogs
* Updates to channel webpages
* Global events (virtual or otherwise)
* Joint channel partner content creation
* Campaigns (channel related)
* And More

### How to ask for work/assistance from Channel Marketing:

*Channel Marketing's inital issues response SLA is 48 hours.*
To start the Channel Marketing support process, 
* [Create an issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#channel_partner_request) 
* This issue template `channel_partner_request` is available in the Partner Markerting project on gitlab.com 
* Complete all sections of the issue template and submit...next steps are stated in the issue

### Channel Marketing go-to-market (GTM) options and requirements for **Select** channel partners

As CAMs on-board new partners there are a few things that are required to have in place for Channel Marketing to support go-to-market / lead gen activities. Our guidance from Channel leadership is to focus in on your business plan with your **Select** Track partners as that should yield the highest return on investment (ROI)


### When to reach out to Channel Marketing for GTM support

At a minimum you will need the following completed and signed off prior to suggesting or scheduling GTM activities with the Channel partners:
1. Partner has a fully executed contract in place
1. Partner has been fully onboarded and certified for both sales and technical
1. In most cases the partner and CAM should have a working business plan for the next 2 quarters to get a solid understanding of what and where the **Select** partner will be focusing in on (what kind of practices will they be standing up: DevOps, DevSecOps, GitOps, etc.) 

For **Select** partners, please feel free to bring Channel Marketing/Field Marketing in your business planning meeting(s) around how GTM. This will help you expand and get creative with your go-to-market activities outside of the 'let's do a webcast' together. There are many other options to support GTM lead generation and the earlier we can get an understanding of this, the more success this activities will be. 

Once you have the 3 above mentioned requirements completed and you need to assist the channel partner (**Open** or **Select**) with launching their partnership here are some ideas for guidance that you can provide :

#### Partner needs support of their press release (PR) with a quote from GitLab
We have a standard [press pelease template](https://drive.google.com/file/d/1XvGWSqo6uOVZTR1Co4Af7PSNSLsGTOWv/view?usp=sharing) located via the partner portal that you can provide to your partner as a foundation for their press release/announcements. The template release includes quotes from both Brandon Jung (Alliances) and Michelle Hodges (Channel), partner to choose executive quote based on partner type. 

* Once your partner completes the press release draft, please create a [PR Channel or Tech Partner Announcement Request issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=channel-partner-announcement-request) for coordination with the Corporate Communications/PR team on reviews and routing through the internal approval process. 

#### Partner wants help with demand generation: 
* Guide the partner to host a webcast/workshop on their platform and work with them on the title and abstract for them to focus on. 
     + Assist them in finding a GitLab presenter (can be the CAM or Channel SA) to speak at the event. 
     + If you need assistance in finding a resource to speak at this event or you would like to add it to our about.gitlab.com/events page, please [Create an issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#channel_partner_request)  for Channel Marketing to assist in sourcing speaker and/or add it to our events page
* Guide the partner to write a blog on their website in support of the launch or joint solution.
     + In some cases, we may be able to support their blog with a quote from GitLab. If this is needed, please create a please create a [PR Channel or Tech Partner Announcement issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new#channel-partner-announcement-request)
     + One way to boost the readability of their blog on their webpage is to create a quick > 10 min overview video on GitLab Unfiltered with the CAM and/or Channel SA and the partner casually providing an overview of the partnership and the value we jointly are bringing to the table with the solution. See instruction for [Uploading conversations to YouTube](/handbook/marketing/marketing-operations/youtube/#uploading-conversations-to-youtube). Once the video clip is complete you can upload it to GitLab Unfiltered and the partner can embed this video into thier blog.
* Obtain the new partners social media channels, go to #social_media and let them know your partners info and if you happen to know when they will be tweeting about their event/announcement etc related to our GitLab Partnership, they will make sure to retweet and reshare as needed.

#### Looking to help your partner host a meetup?
While meetups are more of an awareness opporunity than demand gen, they are a great way to create regional awareness of our joint offerings with our Channel partners. The CAM is responsible for guiding our channel partners to leverage [virtual meetups](https://about.gitlab.com/community/virtual-meetups/) and [live in-person meetups](https://about.gitlab.com/community/meetups/)
    
## 💰 Channel Partner MDF Program

The MDF will be allocated for the region, by proposal. No regional allocation for now. Channel Marketing will allocate MDF for PubSec (Carahsoft will get a standard % of that allocation and the rest will be used as MDF proposals come through)

- PubSec will recieve their amount of MDF allocated to them (likely on a quarterly basis) and all other regions will be [proposal based](https://docs.google.com/document/d/1Dj2vmcSrHyntD-4l6Xjc0GfSSFsK4Knp-xFphObkDXE/edit?usp=sharing) first come, first served basis. 
    + PubSecs number will be sent to FMM NORAM Manager + FMM WW Director via a quarterly MDF Epic - PMM NORAM Manager will provide the PubSec Field team the appropriate information
- As MDF proposals are submitted through the partner portal, Tina Sturgis, Kim Jaeger and Leslie Blanchard will be notified only of the request
    + The Channel Marketing DRI will create a confidential new issue REQUEST INFO HERE for the review and approval process for the MDF proposal - This kicks off the official process
       +  All documentation, analysis, thoughts and approvals will be captured within that issue
       +  Each issue will be associated to the appropriate `Quarterly MDF Epic`
       +  Channel Marketing DRI will establish the appropriate GitLab team member who will be responsible for each of the 3 levels of approval
    +  The MDF proposal will go through 3 levels of approvals 
       + Level 1 approval - Channel Account Manager (CAM)
       + Level 2 approval - Channel Marketing
       + Level 3 approval - Field Marketing
          + For efficiency purposes, Channel Marketing will assign the Level 3 approver for FMM to the appropriate FMM Manager for the region and PubSec
          + The FMM Manager will then allocate to the appropriate FMM based on the CAM/FMM mapping established by FMM and Channel Marketing {details and link to come}
    +  Once the MDF proposal has been either approved or declined the partner will be notified
          + If declined, the Channel Marketing DRI will reach out to the partner and let them know. We will copy the CAM on the notificiation
          + If approved, the FMM will be the DRI for reaching out to the partner contact in the proposal to review dates of execution and provide any other guidance


### Which channel marketing manager should I contact?

- Listed below are areas of responsibility within the channel marketing team:

  - TBH, Channel Marketing Manager
  - [Tina Sturgis](/company/team/#t_sturgis), Manager, Partner and Channel Marketing
  
